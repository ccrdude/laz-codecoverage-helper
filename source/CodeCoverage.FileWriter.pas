{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-13  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit CodeCoverage.FileWriter;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   contnrs,
   Laz2_DOM;

type
   TOnUnknownMethodEncounteredEvent = procedure(const AFilename, AClassName, AMethodName: string) of object;
   TClassNodes = array of TDOMElement;

   TStatusTripple = record
      Tested: integer;
      Total: integer;
      Percent: integer;
   end;

   { TCodeCoverageXML }

   TCodeCoverageXML = class(TXMLDocument)
   private
      FExecutableCountsAsCoverage: boolean;
      FOnUnknownMethodEncountered: TOnUnknownMethodEncounteredEvent;
      FRootNode: TDOMNode;
      FSourceFilename: string;
      FTotalsNode: TDOMNode;
      FTotalsClassesNode: TDOMElement;
      FTotalsMethodsNode: TDOMElement;
      FTotalsFunctionsNode: TDOMElement;
      FTotalsClassesCount: integer;
      FTotalsMethodsCount: integer;
      FTotalsFunctionsCount: integer;
      function GetClassesTotal: TStatusTripple;
      function GetMethodsTotal: TStatusTripple;
   protected
      { Increments the class counter, updating the total field. }
      procedure IncrementClassesCount;
      procedure IncrementMethodsCount; /// tralala
      procedure IncrementFunctionsCount;
   public
      constructor Create;
      procedure Save;
      function GetPrimaryFileNode: TDOMNode;
      function ListClassNodes(AFileNode: TDOMNode): TClassNodes;
      function FindFileNode(AFilename: string; ACreateIfMissing: boolean = True): TDOMNode;
      function FindClassNode(AFileNode: TDOMNode; AClassName: string; ACreateIfMissing: boolean = True; ALineStart: integer = 0): TDOMNode;
      function FindFunctionNode(AFileNode: TDOMNode; AFunctionName: string): TDOMNode;
      function FindMethodNode(AClassNode: TDOMNode; AMethodName: string): TDOMNode;
      function FindSourceLineNode(AFilename: string; ALineNumber: integer): TDOMNode;
      function FindCoverageLineNode(AFilename: string; ALineNumber: integer): TDOMNode;
      function AddSourceLineToken(AFilename: string; ALineNumber: integer; ATokenName, ATokenValue: string): TDOMNode;
      function AddCoverageLineToken(AFilename: string; ALineNumber: integer; ACoveringTestMethod: string = ''): TDOMNode;
      function AddMethod(AFileNode: TDOMNode; AMethodName, ADeclaration: string; ALineStart: integer = 0): TDOMNode;
      procedure RegisterMethodCoveringCode(AFilename, AClassName, AMethodName: string; ATestMethodName: string = '');
      procedure RegisterFunctionCoveringCode(AFilename, AFunctionName: string; ATestMethodName: string = '');
      function FindCoverageNode(AFileNode: TDOMNode): TDOMNode;
      function FindSourceNode(AFileNode: TDOMNode): TDOMNode;
      procedure GetClassTotals(AClassNode: TDOMElement; out AMethodsTotal, AMethodsTested, AMethodsCoverage: integer);
      procedure UpdateClassesCoverage(AFilename: string);
      property ClassesTotal: TStatusTripple read GetClassesTotal;
      property MethodsTotal: TStatusTripple read GetMethodsTotal;
      property RootNode: TDOMNode read FRootNode;
      property SourceFilename: string read FSourceFilename;
      property ExecutableCountsAsCoverage: boolean read FExecutableCountsAsCoverage write FExecutableCountsAsCoverage;
      property OnUnknownMethodEncountered: TOnUnknownMethodEncounteredEvent read FOnUnknownMethodEncountered write FOnUnknownMethodEncountered;
   end;

   { TCodeCoverageFiles }

   TCodeCoverageFiles = class(TObjectList)
   private
      function GetFile(AIndex: integer): TCodeCoverageXML;
   public
      function FindDocumentByFilename(AFilename: string; AExecutableCountsAsCoverage: boolean; ACreateIfMissing: boolean = True): TCodeCoverageXML;
      property Files[AIndex: integer]: TCodeCoverageXML read GetFile;
      procedure Save;
   end;


implementation

uses
   SHA1,
   laz2_XMLWrite;

{ TCodeCoverageFiles }

function TCodeCoverageFiles.GetFile(AIndex: integer): TCodeCoverageXML;
begin
   Result := TCodeCoverageXML(GetItem(AIndex));
end;

function TCodeCoverageFiles.FindDocumentByFilename(AFilename: string; AExecutableCountsAsCoverage: boolean; ACreateIfMissing: boolean): TCodeCoverageXML;
var
   i: integer;
begin
   Result := nil;
   for i := 0 to Pred(Count) do begin
      if SameText(Files[i].SourceFilename, AFilename) then begin
         Result := Files[i];
         Exit;
      end;
   end;
   if ACreateIfMissing and (not Assigned(Result)) then begin
      Result := TCodeCoverageXML.Create;
      Result.FSourceFilename := AFilename;
      Result.ExecutableCountsAsCoverage := AExecutableCountsAsCoverage;
      Self.Add(Result);
   end;
end;

procedure TCodeCoverageFiles.Save;
var
   i: integer;
begin
   for i := 0 to Pred(Count) do begin
      Files[i].Save;
   end;
end;

{ TCodeCoverageXML }

function TCodeCoverageXML.GetClassesTotal: TStatusTripple;
begin
   Result.Total := StrToIntDef(FTotalsClassesNode.GetAttribute('total'), 0);
   Result.Tested := StrToIntDef(FTotalsClassesNode.GetAttribute('tested'), 0);
   Result.Percent := StrToIntDef(FTotalsClassesNode.GetAttribute('percent'), 0);
end;

function TCodeCoverageXML.GetMethodsTotal: TStatusTripple;
begin
   Result.Total := StrToIntDef(FTotalsMethodsNode.GetAttribute('total'), 0);
   Result.Tested := StrToIntDef(FTotalsMethodsNode.GetAttribute('tested'), 0);
   Result.Percent := StrToIntDef(FTotalsMethodsNode.GetAttribute('percent'), 0);
end;

{
  Increments the class counter, updating the total field.
}
procedure TCodeCoverageXML.IncrementClassesCount;
begin
   Inc(FTotalsClassesCount);
   TDOMElement(FTotalsClassesNode).SetAttribute('total', IntToStr(FTotalsClassesCount));
end;

{
  Increments the method counter, updating the total field.
}
procedure TCodeCoverageXML.IncrementMethodsCount;
begin
   Inc(FTotalsMethodsCount);
   TDOMElement(FTotalsMethodsNode).SetAttribute('total', IntToStr(FTotalsMethodsCount));
end;

{
  Increments the function counter, updating the total field.
}
procedure TCodeCoverageXML.IncrementFunctionsCount;
begin
   Inc(FTotalsFunctionsCount);
   TDOMElement(FTotalsFunctionsNode).SetAttribute('total', IntToStr(FTotalsFunctionsCount));
end;

constructor TCodeCoverageXML.Create;
begin
   inherited Create;
   FExecutableCountsAsCoverage := False;
   FRootNode := Self.CreateElement('fpcunit');
   Self.AppendChild(FRootNode);
   FRootNode := Self.DocumentElement;
end;

procedure TCodeCoverageXML.Save;
begin
   WriteXMLFile(Self, FSourceFilename + '.xml');
end;

function TCodeCoverageXML.GetPrimaryFileNode: TDOMNode;
begin
   Result := FindFileNode(FSourceFilename);
end;

function TCodeCoverageXML.ListClassNodes(AFileNode: TDOMNode): TClassNodes;
var
   iFileChild, iCount: integer;
   e: TDOMElement;
begin
   SetLength(Result, 0);
   for iFileChild := 0 to Pred(AFileNode.ChildNodes.Count) do begin
      if AFileNode.ChildNodes[iFileChild].NodeName = 'class' then begin
         e := TDOMElement(AFileNode.ChildNodes[iFileChild]);
         iCount := Length(Result);
         SetLength(Result, Succ(iCount));
         Result[iCount] := e;
      end;
   end;
end;

function TCodeCoverageXML.FindFileNode(AFilename: string; ACreateIfMissing: boolean): TDOMNode;
var
   iFile: integer;
   e: TDOMElement;
begin
   Result := nil;
   for iFile := 0 to Pred(FRootNode.ChildNodes.Count) do begin
      if FRootNode.ChildNodes[iFile].NodeName = 'file' then begin
         e := TDOMElement(FRootNode.ChildNodes[iFile]);
         if SameText(e.GetAttribute('name'), ExtractFilename(AFilename)) and SameText(e.GetAttribute('path'), ExtractFilePath(AFilename)) then begin
            Result := e;
            Exit;
         end;
      end;
   end;
   if not ACreateIfMissing then begin
      Exit;
   end;
   if not Assigned(Result) then begin
      Result := Self.CreateElement('file');
      TDOMElement(Result).SetAttribute('name', ExtractFileName(AFilename));
      TDOMElement(Result).SetAttribute('path', ExtractFilePath(AFilename));
      TDOMElement(Result).SetAttribute('sha1', LowerCase(SHA1Print(SHA1File(AFilename))));
      RootNode.AppendChild(Result);

      FTotalsNode := Self.CreateElement('totals');
      Result.AppendChild(FTotalsNode);
      FTotalsClassesNode := Self.CreateElement('classes');
      TDOMElement(FTotalsClassesNode).SetAttribute('total', '0');
      TDOMElement(FTotalsClassesNode).SetAttribute('tested', '0');
      TDOMElement(FTotalsClassesNode).SetAttribute('percent', '0');
      FTotalsNode.AppendChild(FTotalsClassesNode);
      FTotalsMethodsNode := Self.CreateElement('methods');
      TDOMElement(FTotalsMethodsNode).SetAttribute('total', '0');
      TDOMElement(FTotalsMethodsNode).SetAttribute('tested', '0');
      TDOMElement(FTotalsMethodsNode).SetAttribute('percent', '0');
      FTotalsNode.AppendChild(FTotalsMethodsNode);
      FTotalsFunctionsNode := Self.CreateElement('functions');
      TDOMElement(FTotalsFunctionsNode).SetAttribute('total', '0');
      TDOMElement(FTotalsFunctionsNode).SetAttribute('tested', '0');
      TDOMElement(FTotalsFunctionsNode).SetAttribute('percent', '0');
      FTotalsNode.AppendChild(FTotalsFunctionsNode);
   end;
end;

function TCodeCoverageXML.FindClassNode(AFileNode: TDOMNode; AClassName: string; ACreateIfMissing: boolean; ALineStart: integer): TDOMNode;
var
   iFileChild: integer;
   e: TDOMElement;
begin
   Result := nil;
   for iFileChild := 0 to Pred(AFileNode.ChildNodes.Count) do begin
      if AFileNode.ChildNodes[iFileChild].NodeName = 'class' then begin
         e := TDOMElement(AFileNode.ChildNodes[iFileChild]);
         if SameText(e.GetAttribute('name'), AClassName) then begin
            Result := e;
            Exit;
         end;
      end;
   end;
   if not ACreateIfMissing then begin
      Exit;
   end;
   if not Assigned(Result) then begin
      Result := Self.CreateElement('class');
      TDOMElement(Result).SetAttribute('name', AClassName);
      TDOMElement(Result).SetAttribute('start', IntToStr(ALineStart));
      TDOMElement(Result).SetAttribute('executable', '0');
      TDOMElement(Result).SetAttribute('coverage', '0');
      AFileNode.AppendChild(Result);
      IncrementClassesCount;
   end;
end;

function TCodeCoverageXML.FindFunctionNode(AFileNode: TDOMNode; AFunctionName: string): TDOMNode;
var
   iFileChild: integer;
   e: TDOMElement;
begin
   Result := nil;
   for iFileChild := 0 to Pred(AFileNode.ChildNodes.Count) do begin
      if AFileNode.ChildNodes[iFileChild].NodeName = 'function' then begin
         e := TDOMElement(AFileNode.ChildNodes[iFileChild]);
         if SameText(e.GetAttribute('name'), AFunctionName) then begin
            Result := e;
            Exit;
         end;
      end;
   end;
end;

function TCodeCoverageXML.FindMethodNode(AClassNode: TDOMNode; AMethodName: string): TDOMNode;
var
   iMethod: integer;
   e: TDOMElement;
begin
   Result := nil;
   for iMethod := 0 to Pred(AClassNode.ChildNodes.Count) do begin
      if AClassNode.ChildNodes[iMethod].NodeName = 'method' then begin
         e := TDOMElement(AClassNode.ChildNodes[iMethod]);
         if SameText(e.GetAttribute('name'), AMethodName) then begin
            Result := e;
            Exit;
         end;
      end;
   end;
end;

function TCodeCoverageXML.FindSourceLineNode(AFilename: string; ALineNumber: integer): TDOMNode;
var
   iLine: integer;
   nCoverage: TDOMNode;
   e: TDOMElement;
begin
   Result := nil;
   nCoverage := FindSourceNode(FindFileNode(AFilename));
   for iLine := 0 to Pred(nCoverage.ChildNodes.Count) do begin
      if nCoverage.ChildNodes[iLine].NodeName = 'line' then begin
         e := TDOMElement(nCoverage.ChildNodes[iLine]);
         if SameText(e.GetAttribute('nr'), IntToStr(ALineNumber)) then begin
            Result := e;
            Exit;
         end;
      end;
   end;
   if not Assigned(Result) then begin
      Result := Self.CreateElement('line');
      TDOMElement(Result).SetAttribute('nr', IntToStr(ALineNumber));
      nCoverage.AppendChild(Result);
   end;
end;

function TCodeCoverageXML.FindCoverageLineNode(AFilename: string; ALineNumber: integer): TDOMNode;
var
   iLine: integer;
   nCoverage: TDOMNode;
   e: TDOMElement;
begin
   Result := nil;
   nCoverage := FindCoverageNode(FindFileNode(AFilename));
   for iLine := 0 to Pred(nCoverage.ChildNodes.Count) do begin
      if nCoverage.ChildNodes[iLine].NodeName = 'line' then begin
         e := TDOMElement(nCoverage.ChildNodes[iLine]);
         if SameText(e.GetAttribute('nr'), IntToStr(ALineNumber)) then begin
            Result := e;
            Exit;
         end;
      end;
   end;
   if not Assigned(Result) then begin
      Result := Self.CreateElement('line');
      TDOMElement(Result).SetAttribute('nr', IntToStr(ALineNumber));
      nCoverage.AppendChild(Result);
   end;
end;

function TCodeCoverageXML.AddSourceLineToken(AFilename: string; ALineNumber: integer; ATokenName, ATokenValue: string): TDOMNode;
var
   nLine: TDOMNode;
   dt: TDOMText;
begin
   nLine := FindSourceLineNode(AFilename, ALineNumber);
   Result := Self.CreateElement('token');
   TDOMElement(Result).SetAttribute('name', ATokenName);
   if Length(ATokenValue) > 0 then begin
      dt := Self.CreateTextNode(ATokenValue);
      Result.AppendChild(dt);
   end;
   nLine.AppendChild(Result);
end;

function TCodeCoverageXML.AddCoverageLineToken(AFilename: string; ALineNumber: integer; ACoveringTestMethod: string): TDOMNode;
var
   nLine: TDOMNode;
begin
   if ALineNumber < 0 then begin
      Exit;
   end;
   nLine := FindCoverageLineNode(AFilename, ALineNumber);
   Result := Self.CreateElement('covered');
   if Length(ACoveringTestMethod) > 0 then begin
      TDOMElement(Result).SetAttribute('by', ACoveringTestMethod);
   end;
   nLine.AppendChild(Result);
end;

function TCodeCoverageXML.AddMethod(AFileNode: TDOMNode; AMethodName, ADeclaration: string; ALineStart: integer): TDOMNode;
var
   iDot: integer;
   sClassName, sMethodName: string;
   nClass: TDOMNode;
begin
   ADeclaration := StringReplace(ADeclaration, #13, ' ', [rfReplaceAll]);
   ADeclaration := StringReplace(ADeclaration, #10, ' ', [rfReplaceAll]);
   while Pos('  ', ADeclaration) > 0 do begin
      ADeclaration := StringReplace(ADeclaration, '  ', ' ', [rfReplaceAll]);
   end;
   iDot := Pos('.', AMethodName);
   if iDot > 0 then begin
      sClassName := Copy(AMethodName, 1, Pred(iDot));
      sMethodName := Copy(AMethodName, Succ(iDot));
      nClass := Self.FindClassNode(AFileNode, sClassName, True, 0);
      Result := FindMethodNode(nClass, sMethodName);
      if not Assigned(Result) then begin
         Result := Self.CreateElement('method');
         TDOMElement(Result).SetAttribute('name', sMethodName);
         nClass.AppendChild(Result);
      end;
      IncrementMethodsCount;
   end else begin
      Result := FindFunctionNode(AFileNode, AMethodName);
      if not Assigned(Result) then begin
         Result := Self.CreateElement('function');
         TDOMElement(Result).SetAttribute('name', AMethodName);
         AFileNode.AppendChild(Result);
      end;
      IncrementFunctionsCount;
   end;
   TDOMElement(Result).SetAttribute('start', IntToStr(ALineStart));
   TDOMElement(Result).SetAttribute('signature', ADeclaration);
   TDOMElement(Result).SetAttribute('coverage', '0');
   TDOMElement(Result).SetAttribute('executable', '0');
end;

procedure TCodeCoverageXML.RegisterMethodCoveringCode(AFilename, AClassName, AMethodName: string; ATestMethodName: string);
var
   nFile: TDOMNode;
   nClass: TDOMElement;
   nMethod: TDOMElement;
   nCoveredBy: TDOMNode;
begin
   nFile := Self.FindFileNode(AFilename);
   nClass := TDOMElement(Self.FindClassNode(nFile, AClassName));
   nClass.SetAttribute('executable', IntToStr(Succ(StrToIntDef(nClass.GetAttribute('executable'), 0))));
   nMethod := TDOMElement(Self.FindMethodNode(nClass, AMethodName));
   if Assigned(nMethod) then begin
      nMethod.SetAttribute('executable', IntToStr(Succ(StrToIntDef(nMethod.GetAttribute('executable'), 0))));
      if FExecutableCountsAsCoverage then begin
         nMethod.SetAttribute('coverage', '100');
      end;
      if Length(ATestMethodName) > 0 then begin
         nCoveredBy := Self.CreateElement('covered');
         TDOMElement(nCoveredBy).SetAttribute('by', ATestMethodName);
         nMethod.AppendChild(nCoveredBy);
      end;
   end else begin
      if Assigned(FOnUnknownMethodEncountered) then begin
         FOnUnknownMethodEncountered(AFilename, AClassName, AMethodName);
      end;
   end;
end;

procedure TCodeCoverageXML.RegisterFunctionCoveringCode(AFilename, AFunctionName: string; ATestMethodName: string);
var
   nFile: TDOMNode;
   nFunction: TDOMElement;
   nCoveredBy: TDOMNode;
begin
   nFile := Self.FindFileNode(AFilename);
   nFunction := TDOMElement(Self.FindFunctionNode(nFile, AFunctionName));
   nFunction.SetAttribute('executable', IntToStr(Succ(StrToIntDef(nFunction.GetAttribute('executable'), 0))));
   if FExecutableCountsAsCoverage then begin
      nFunction.SetAttribute('coverage', '100');
   end;
   if Length(ATestMethodName) > 0 then begin
      nCoveredBy := Self.CreateElement('covered');
      TDOMElement(nCoveredBy).SetAttribute('by', ATestMethodName);
      nFunction.AppendChild(nCoveredBy);
   end;
end;

function TCodeCoverageXML.FindCoverageNode(AFileNode: TDOMNode): TDOMNode;
var
   iFileChild: integer;
begin
   Result := nil;
   for iFileChild := 0 to Pred(AFileNode.ChildNodes.Count) do begin
      if AFileNode.ChildNodes[iFileChild].NodeName = 'coverage' then begin
         Result := AFileNode.ChildNodes[iFileChild];
         Exit;
      end;
   end;
   Result := Self.CreateElement('coverage');
   AFileNode.AppendChild(Result);
end;

function TCodeCoverageXML.FindSourceNode(AFileNode: TDOMNode): TDOMNode;
var
   iFileChild: integer;
begin
   Result := nil;
   for iFileChild := 0 to Pred(AFileNode.ChildNodes.Count) do begin
      if AFileNode.ChildNodes[iFileChild].NodeName = 'source' then begin
         Result := AFileNode.ChildNodes[iFileChild];
         Exit;
      end;
   end;
   Result := Self.CreateElement('source');
   AFileNode.AppendChild(Result);
end;

procedure TCodeCoverageXML.GetClassTotals(AClassNode: TDOMElement; out AMethodsTotal, AMethodsTested, AMethodsCoverage: integer);
var
   iMethod: integer;
   nMethod: TDOMNode;
   iMethodCoverage: integer;
   iMethodCoverageSum: integer;
begin
   AMethodsTotal := 0;
   AMethodsTested := 0;
   iMethodCoverageSum := 0;
   for iMethod := 0 to Pred(AClassNode.ChildNodes.Count) do begin
      nMethod := AClassNode.ChildNodes.Item[iMethod];
      if nMethod.NodeName = 'method' then begin
         Inc(AMethodsTotal);
         iMethodCoverage := StrToIntDef(TDOMElement(nMethod).GetAttribute('coverage'), 0);
         if iMethodCoverage = 100 then begin
            Inc(AMethodsTested);
         end;
         Inc(iMethodCoverageSum, iMethodCoverage);
      end;
   end;
   AMethodsCoverage := Round(iMethodCoverageSum / AMethodsTotal);
end;

procedure TCodeCoverageXML.UpdateClassesCoverage(AFilename: string);
var
   nFile: TDOMNode;
   iFileChild, iMethod: integer;
   e, eMethod: TDOMElement;
   iCoverageSum: integer;
   iCoverageCount: integer;
   iClassCoverage: integer;
   iMethodCoverage: integer;
   iMethodsTested: integer;
   iMethodsUntested: integer;
   iMethodsPercent: integer;
   iClassesTested: integer;
   iClassesUntested: integer;
   iClassesPercent: integer;
begin
   iMethodsTested := 0;
   iMethodsUntested := 0;
   iClassesTested := 0;
   iClassesUntested := 0;
   nFile := FindFileNode(AFilename);
   if not Assigned(nFile) then begin
      Exit;
   end;
   for iFileChild := 0 to Pred(nFile.ChildNodes.Count) do begin
      if nFile.ChildNodes[iFileChild].NodeName = 'class' then begin
         e := TDOMElement(nFile.ChildNodes[iFileChild]);
         iCoverageSum := 0;
         iCoverageCount := 0;
         for iMethod := 0 to Pred(e.ChildNodes.Count) do begin
            eMethod := TDOMElement(e.ChildNodes[iMethod]);
            if eMethod.hasAttribute('coverage') then begin
               iMethodCoverage := StrToIntDef(eMethod.GetAttribute('coverage'), 0);
               Inc(iCoverageCount);
               Inc(iCoverageSum, iMethodCoverage);
               if iMethodCoverage = 100 then begin
                  Inc(iMethodsTested);
               end else begin
                  Inc(iMethodsUntested);
               end;
            end;
         end;
         iClassCoverage := iCoverageSum div iCoverageCount;
         if iClassCoverage = 100 then begin
            Inc(iClassesTested);
         end else begin
            Inc(iClassesUntested);
         end;
         e.SetAttribute('coverage', IntToStr(iClassCoverage));
      end;
   end;
   if (iClassesTested + iClassesUntested) > 0 then begin
      iClassesPercent := Round(100 * (iClassesTested / (iClassesTested + iClassesUntested)));
   end else begin
      iClassesPercent := 0;
   end;
   TDOMElement(FTotalsClassesNode).SetAttribute('tested', IntToStr(iClassesTested));
   TDOMElement(FTotalsClassesNode).SetAttribute('percent', IntToStr(iClassesPercent));
   if (iMethodsTested + iMethodsUntested) > 0 then begin
      iMethodsPercent := Round(100 * (iMethodsTested / (iMethodsTested + iMethodsUntested)));
   end else begin
      iMethodsPercent := 0;
   end;
   TDOMElement(FTotalsMethodsNode).SetAttribute('tested', IntToStr(iMethodsTested));
   TDOMElement(FTotalsMethodsNode).SetAttribute('percent', IntToStr(iMethodsPercent));
   Save;
end;

end.
