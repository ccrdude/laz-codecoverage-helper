{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Parses Gnu Debugger files for source unit paths.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-14  pk  30m  Created unit.
// *****************************************************************************
   )
}

unit CodeCoverage.GDB;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Windows;

type

   { TGnuDebuggerFile }

   TGnuDebuggerFile = class
   private
      FStabStrings: TStringList;
      FFilename: string;
      {
        Processes the content of the .stabstr section of the debugger file.

        @param(AStream A stream containing the section content.)
      }
      function ProcessStabStr(AStream: TStream): boolean;
   public
      constructor Create;
      destructor Destroy; override;
      {
        Load information from a Gnu Debugger (.gdb) file.

        @param(AFilename The name of the .gdb file to load.)
      }
      function LoadFromFile(AFilename: string): boolean;
      {
        Tries to find the full path of a unit.

        @param(AUnitName Takes the unit name, and returns it with path and appended .pas.)
        @return(True if unit was found.)
      }
      function GetUnitFullFilename(var AUnitName: string): boolean;
      {
        ListUnitFilenames returns all existing unit files referenced in the
        .gdb file.
      }
      procedure ListUnitFilenames(AList: TStrings);
      property StabStrings: TStringList read FStabStrings;
   end;

implementation

uses
   imagehlp;

{
  SectionNameToString converts an array of bytes of max. 8 bytes, as seen in
  Windows PE section tables, into a string.

  @param(ANameBytes is the input array.)
  @return(String representation of section name.)
}
function SectionNameToString(ANameBytes: array of byte): ansistring;
var
   i: integer;
begin
   Result := '';
   for i := 0 to 7 do begin
      if ANameBytes[i] <> 0 then begin
         Result := Result + Chr(ANameBytes[i]);
      end else begin
         Exit;
      end;
   end;
end;

function RVAToPointer(ARelativeVirtualAddress: DWORD; const AnImage: TLOADED_IMAGE): Pointer;
var
   pDummy: PPIMAGE_SECTION_HEADER;
begin
   pDummy := nil;
   Result := ImageRvaToVa(AnImage.FileHeader, AnImage.MappedAddress, ARelativeVirtualAddress, pDummy);
end;

{ TGnuDebuggerFile }

function TGnuDebuggerFile.ProcessStabStr(AStream: TStream): boolean;
var
   //sWorking: ansistring;
   //c: AnsiChar;
   ss: TStringStream;
begin
   FStabStrings.Clear;
   ss := TStringStream.Create('');
   AStream.Seek(0, soFromBeginning);
   try
      ss.CopyFrom(AStream, AStream.Size);
      // alt 1
      //FStabStrings.Delimiter := #0;
      //FStabStrings.StrictDelimiter := True;
      //FStabStrings.DelimitedText := ss.DataString;
      // alt 2
      FStabStrings.Text := StringReplace(ss.DataString, #0, LineEnding, [rfReplaceAll]);
      Result := true;
   finally
      ss.Free;
   end;
   (*
   sWorking := '';
   c := #0;
   while AStream.Position < AStream.Size do begin
      AStream.Read(c, 1);
      if c = #0 then begin
         FStabStrings.Add(sWorking);
         sWorking := '';
      end else begin
         sWorking := sWorking + c;
      end;
   end;
   FStabStrings.Add(sWorking);
   Result := True;
   *)
end;

constructor TGnuDebuggerFile.Create;
begin
   FStabStrings := TStringList.Create;
end;

destructor TGnuDebuggerFile.Destroy;
begin
   FStabStrings.Free;
   inherited Destroy;
end;

function TGnuDebuggerFile.LoadFromFile(AFilename: string): boolean;
var
   pi: PLOADED_IMAGE;
   pish: PIMAGE_SECTION_HEADER;
   i: integer;
   sSection: string;
   ms: TMemoryStream;
begin
   Result := False;
   FFilename := AFilename;
   pi := ImageLoad(PChar(ExtractFileName(AFilename)), PChar(ExtractFilePath(AFilename)));
   if Assigned(pi) then begin
      try
         for i := 0 to Pred(pi^.FileHeader^.FileHeader.NumberOfSections) do begin
            pish := pi^.Sections;
            Inc(pish, i);
            sSection := SectionNameToString(pish^.Name);
            if SameText(sSection, '.stabstr') then begin
               ms := TMemoryStream.Create;
               try
                  ms.Size := pish^.SizeOfRawData;
                  Move(RVAToPointer(pish^.VirtualAddress, pi^)^, ms.Memory^, ms.Size);
                  ms.Seek(0, soFromBeginning);
                  Result := ProcessStabStr(ms);
               finally
                  ms.Free;
               end;
            end;
         end;
      finally
         ImageUnload(pi);
      end;
   end;
end;

function TGnuDebuggerFile.GetUnitFullFilename(var AUnitName: string): boolean;
var
   i: integer;
begin
   try
      i := FStabStrings.IndexOf(AUnitName + '.pas');
      Result := (i > 0);
      if Result then begin
         AUnitName := FStabStrings[Pred(i)] + AUnitName + '.pas';
         Exit;
      end;
      i := FStabStrings.IndexOf(AUnitName + '.pp');
      Result := (i > 0);
      if Result then begin
         AUnitName := FStabStrings[Pred(i)] + AUnitName + '.pp';
         Exit;
      end;
   finally
      if DirectorySeparator <> '/' then begin
         AUnitName := StringReplace(AUnitName, '/', DirectorySeparator, [rfReplaceAll]);
      end;
   end;
end;

procedure TGnuDebuggerFile.ListUnitFilenames(AList: TStrings);
var
   i: integer;
   sUnitName: string;
begin
   for i := 0 to FStabStrings.Count - 2 do begin
      if (Copy(FStabStrings[i], 1, 1) = '/') or (Copy(FStabStrings[i], 2, 2) = ':/') then begin
         sUnitName := FStabStrings[i] + FStabStrings[Succ(i)];
         if FileExists(sUnitName) then begin
            if DirectorySeparator <> '/' then begin
               sUnitName := StringReplace(sUnitName, '/', DirectorySeparator, [rfReplaceAll]);
            end;
            AList.Add(sUnitName);
         end;
      end;
   end;
   (*
   // This code requires that any first appearance of a filename is
   // happening right after the path listing.
   for i := 1 to Pred(FStabStrings.Count) do begin
      if SameText('.PAS', RightStr(FStabStrings[i], 4)) or SameText('.PP', RightStr(FStabStrings[i], 3)) then begin
         sUnitName := FStabStrings[Pred(i)] + FStabStrings[i];
         if FileExists(sUnitName) then begin
            {$IFDEF MSWindows}
            sUnitName := StringReplace(sUnitName, '/', '\', [rfReplaceAll]);
            {$ENDIF MSWindows}
            AList.Add(sUnitName);
         end;
      end;
   end;
   *)
end;

end.
