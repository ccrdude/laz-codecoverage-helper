{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Code Coverage class specific to FPCUnit.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-18  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit CodeCoverage.FPCUnit;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   fpcunit,
   CodeCoverage,
   CodeCoverage.GDB;

type

   { TCodeCoverageFPCUnit }

   TCodeCoverageFPCUnit = class(TCodeCoverage)
   protected
      {
        GetTestClassNames retrieves the names of test classes.

        @param(ANames Will be filled with names of classes.)
      }
      procedure GetTestClassNames(ANames: TStringList); override;
      {
        FindTestUnitFilenames overrides the original method by one that gathers
        information from the TestRegistry.

        @param(AGnuDebuggerFile A class holding information from the .gdb file.)
      }
      procedure FindTestUnitFilenames(AGnuDebuggerFile: TGnuDebuggerFile); override;
   end;

implementation

uses
   testregistry;

procedure TCodeCoverageFPCUnit.GetTestClassNames(ANames: TStringList);
var
   ts: TTestSuite;
   tClass, tMethod: TTest;
   iTest, iMethod: integer;
begin
   ts := GetTestRegistry;
   for iTest := 0 to Pred(ts.GetChildTestCount) do begin
      tClass := ts.GetChildTest(iTest);
      if tClass.GetChildTestCount > 0 then begin
         for iMethod := 0 to Pred(tClass.GetChildTestCount) do begin
            tMethod := tClass.GetChildTest(iMethod);
            if iMethod = 0 then begin
               ANames.Add(tMethod.ClassName);
            end;
            TestMethods.Add(tMethod.ClassName + '.' + tMethod.TestName);
         end;
      end else begin
         ANames.Add(tClass.TestName);
      end;
   end;
end;

procedure TCodeCoverageFPCUnit.FindTestUnitFilenames(AGnuDebuggerFile: TGnuDebuggerFile);
var
   ts: TTestSuite;
   tSuite, tMethod: TTest;
   iTest, iMethod: integer;
   sUnitName: string;
begin
   ts := GetTestRegistry;
   for iTest := 0 to Pred(ts.GetChildTestCount) do begin
      tSuite := ts.GetChildTest(iTest);
      if tSuite.GetChildTestCount > 0 then begin
         for iMethod := 0 to Pred(tSuite.GetChildTestCount) do begin
            tMethod := tSuite.GetChildTest(iMethod);
            if iMethod = 0 then begin
               sUnitName := tMethod.UnitName;
               if AGnuDebuggerFile.GetUnitFullFilename(sUnitName) then begin
                  TestCaseUnits.Add(sUnitName);
               end;
            end;
            TestMethods.Add(tMethod.ClassName + '.' + tMethod.TestName);
         end;
      end;
   end;
   TestMethods.SaveToFile(ParamStr(0) + '.testMethods.lst');
end;

end.
