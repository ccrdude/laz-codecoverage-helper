unit TestDebuggerForm;

{$mode objfpc}{$H+}
{$APPTYPE CONSOLE}

interface

uses
   Classes,
   SysUtils,
   FileUtil,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   StdCtrls,
   ComCtrls,
   CodeCoverage.Debugger,
   SynEdit,
   SynEditMiscClasses;

type

   { TForm1 }

   TForm1 = class(TForm)
      bnGo: TButton;
      cbFullOutput: TCheckBox;
      cbExe: TComboBox;
      memoLinesOnly: TSynEdit;
      memoLines: TSynEdit;
      memoBreakpoints: TSynEdit;
      PageControl1: TPageControl;
      tabAll: TTabSheet;
      tabBreakpoints: TTabSheet;
      tabLines: TTabSheet;
      procedure bnGoClick(Sender: TObject);
      procedure cbFullOutputChange(Sender: TObject);
      procedure FormCreate(Sender: TObject);
      procedure memoLinesSpecialLineColors(Sender: TObject; Line: integer; var Special: boolean; var FG, BG: TColor);
   private
      FStart: TDateTime;
      FDebugger: TCodeCoverageDebugger;
      procedure BreakpointLineCopy;
      procedure DoLineDebug(const AFilename: string; ALine: integer);
   public

   end;

var
   Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
   FDebugger := TCodeCoverageDebugger.Create;
   FDebugger.DebuggerExecutable := 'c:\Development\lazarus-1.7\fpcbootstrap\gdb\i386-win32\gdb.exe';
   FDebugger.OnLineDebug := @DoLineDebug;
end;

procedure TForm1.memoLinesSpecialLineColors(Sender: TObject; Line: integer; var Special: boolean; var FG, BG: TColor);
var
   s: string;
begin
   s := memoLines.Lines[Line - 1];
   if LeftStr(s, 8) = 'Output: ' then begin
      Special := True;
      FG := clDkGray;
   end else if LeftStr(s, 7) = 'Event: ' then begin
      Special := True;
      FG := clMaroon;
   end else if LeftStr(s, 12) = 'Breakpoint: ' then begin
      Special := True;
      FG := clRed;
   end else if Copy(s, 2, 2) = ':\' then begin
      Special := True;
      BG := clLtGray;
      FG := clWhite;
   end;
   if (Pos('-break-', s) > 0) or (Pos('breakpoint', s) > 0) then begin
      Special := True;
      BG := clYellow;
   end;
end;

procedure TForm1.BreakpointLineCopy;
var
   i: integer;
   s: string;
begin
   for i := 0 to Pred(memoLines.Lines.Count) do begin
      s := memoLines.Lines[i];
      if (Pos('-break-', s) > 0) or (Pos('breakpoint', s) > 0) then begin
         memoBreakpoints.Lines.Add(s);
      end;
   end;
end;

procedure TForm1.DoLineDebug(const AFilename: string; ALine: integer);
begin
   memoLines.Lines.Add(AFilename + ':' + IntToStr(ALine));
   memoLinesOnly.Lines.Add(AFilename + ':' + IntToStr(ALine));
end;

procedure TForm1.bnGoClick(Sender: TObject);
var
   sFilename: string;
begin
   FStart := Now;
   sFilename := cbExe.Text;
   FDebugger.ProduceFullOutput := cbFullOutput.Checked;
   FDebugger.Go(sFilename);
   memoLines.Lines.Clear;
   memoBreakpoints.Lines.Clear;
   memoLinesOnly.Lines.Clear;
   if FDebugger.ProduceFullOutput then begin
      memoLines.Lines.Assign(FDebugger.FullProtocol);
      memoLinesOnly.Lines.Assign(FDebugger.LineProtocol);
      BreakpointLineCopy;
   end else begin
      memoLinesOnly.Lines.Assign(FDebugger.LineProtocol);
   end;
   Self.Caption := 'Debugging took ' + FormatDateTime('hh:nn:ss', Now - FStart);
end;

procedure TForm1.cbFullOutputChange(Sender: TObject);
begin
   tabBreakpoints.TabVisible := cbFullOutput.Checked;
   tabAll.TabVisible := cbFullOutput.Checked;
end;

end.
