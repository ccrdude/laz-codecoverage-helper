{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Enhances GuiTestRunner form with Code Coverage information.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// Icons © Lazarus 1.9 SVN
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-14  pk  ---  Added header and some documentation.
// 2017-06-13  pk  ---  Wrote first working version with tabs displaying something.
// *****************************************************************************
   )
}

unit CodeCoverage.FPCUnit.GuiTestRunner;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   fpcunit,
   Forms,
   Controls,
   ComCtrls,
   Laz2_DOM,
   GuiTestRunner,
   CodeCoverage,
   CodeCoverage.UnitFrame,
   CodeCoverage.FPCUnit;

type

   { TCodeCoverageTabSheet }

   TCodeCoverageTabSheet = class(TTabSheet)
   protected
      FFrame: TFrameCodeCoverage;
   end;

   { TCodeCoverageGUITestRunner }

   TCodeCoverageGUITestRunner = class // (TGUITestRunner)
   private
      FCodeCoverage: TCodeCoverage;
      {
        DisplayCodeCoverage inserts code coverage information into the
        GuiTestRunner user interface.
      }
      procedure DisplayCodeCoverage(const APageControl: TPageControl);
      {
        ClearTabs removes all code coverage information tabs from the
        GuiTestRunner user interface.
      }
      procedure ClearTabs(const APageControl: TPageControl);
      {
        GetTab returns the code coverage information tab for the specified
        file.
      }
      function GetTab(const APageControl: TPageControl; const AFilename: string): TCodeCoverageTabSheet;
   public
      constructor Create; // (TheOwner: TComponent); override;
      destructor Destroy; override;
      procedure DoBeforeRunTest(AGUITestRunnerForm: TGUITestRunner);
      procedure DoAfterRunTest(AGUITestRunnerForm: TGUITestRunner);
      // procedure RunTest(ATest: TTest); override;
   private
      class var FInstance: TCodeCoverageGUITestRunner;
   public
      class constructor Create;
      class destructor Destroy;
   end;

implementation

uses
   Graphics,
   testregistry;

{ TCodeCoverageGUITestRunner }

procedure TCodeCoverageGUITestRunner.DisplayCodeCoverage(const APageControl: TPageControl);
var
   i: integer;
begin
   for i := 0 to Pred(FCodeCoverage.Documents.Count) do begin
      GetTab(APageControl, FCodeCoverage.Documents.Files[i].SourceFilename);
   end;
end;

procedure TCodeCoverageGUITestRunner.ClearTabs(const APageControl: TPageControl);
var
   i: integer;
   t: TCodeCoverageTabSheet;
begin
   for i := Pred(APageControl.PageCount) downto 2 do begin
      if APageControl.Pages[i] is TCodeCoverageTabSheet then begin
         t := TCodeCoverageTabSheet(APageControl.Pages[i]);
         t.PageControl := nil;
         t.Parent := nil;
         t.Free;
      end;
   end;
end;

function TCodeCoverageGUITestRunner.GetTab(const APageControl: TPageControl; const AFilename: string): TCodeCoverageTabSheet;
begin
   Result := TCodeCoverageTabSheet.Create(nil);
   Result.Parent := APageControl;
   Result.PageControl := APageControl;
   Result.Caption := ExtractFileName(AFilename);
   Result.FFrame := TFrameCodeCoverage.Create(Result);
   Result.FFrame.Parent := Result;
   Result.FFrame.Align := alClient;
   Result.FFrame.ShowUnitCodeCoverage(AFilename);
end;

constructor TCodeCoverageGUITestRunner.Create; // (TheOwner: TComponent);
begin
   inherited Create; // (TheOwner);
   FCodeCoverage := TCodeCoverageFPCUnit.Create;
end;

destructor TCodeCoverageGUITestRunner.Destroy;
begin
   FCodeCoverage.Free;
   inherited Destroy;
end;

procedure TCodeCoverageGUITestRunner.DoBeforeRunTest(AGUITestRunnerForm: TGUITestRunner);
begin
   AGUITestRunnerForm.XMLSynEdit.BorderSpacing.Around := 0;
   AGUITestRunnerForm.TestTree.BorderSpacing.Top := 0;
   AGUITestRunnerForm.PageControl1.BorderSpacing.Around := 6;
   ClearTabs(AGUITestRunnerForm.PageControl1);
   FCodeCoverage.PreProcessCodeCoverage;
end;

procedure TCodeCoverageGUITestRunner.DoAfterRunTest(AGUITestRunnerForm: TGUITestRunner);
begin
   FCodeCoverage.PostProcessCodeCoverage;
   DisplayCodeCoverage(AGUITestRunnerForm.PageControl1);
end;

class constructor TCodeCoverageGUITestRunner.Create;
begin
   FInstance := TCodeCoverageGUITestRunner.Create;
   TGUITestRunner.AddHandlerBeforeRunTest(@FInstance.DoBeforeRunTest, true);
   TGUITestRunner.AddHandlerAfterRunTest(@FInstance.DoAfterRunTest, true);
end;

class destructor TCodeCoverageGUITestRunner.Destroy;
begin
   TGUITestRunner.RemoveHandlerBeforeRunTest(@FInstance.DoBeforeRunTest);
   TGUITestRunner.RemoveHandlerAfterRunTest(@FInstance.DoAfterRunTest);
   FInstance.Free;
end;

//procedure TCodeCoverageGUITestRunner.RunTest(ATest: TTest);
//begin
//   DoBeforeRunTest(Self);
//   inherited RunTest(ATest);
//   DoAfterRunTest(Self);
//end;

end.
