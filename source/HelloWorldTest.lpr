{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-23  --  ---  Renamed from ram HelloWorldTest to HelloWorldTest
// 2017-06-23  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

program HelloWorldTest;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

uses
   Classes,
   TextTestRunner,
   CodeCoverage, // added for Code Coverage
   CodeCoverage.FPTest,
   HelloWorld.Greetings.TestCase;

begin
   with TCodeCoverageFPTest.Create do begin
      PreProcessCodeCoverage; // added for Code Coverage
      TestCaseUnits.Add('c:\Development\Projects\CodingTools\laz-codecoverage-helper\source\HelloWorld.Greetings.TestCase.pas');
      SourceUnits.Add('c:\Development\Projects\CodingTools\laz-codecoverage-helper\source\HelloWorld.Greetings.pas');
      RunRegisteredTests;
      PostProcessCodeCoverage; // added for Code Coverage
      WriteLn(GetResultsAsConsoleOutput); // added for Code Coverage
      Free;
   end;
end.
