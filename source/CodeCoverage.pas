{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Create code coverage reports for the current FPCUnit test suite.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-15  pk   1h  Implemented source units lookup.
// 2017-06-14  pk   1h  Implemented testcase units lookup.
// 2017-06-12  pk  ---  Started draft
// *****************************************************************************
   )
}

unit CodeCoverage;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

{.$DEFINE DebugDumpCodeCoverageInformationLists}

interface

uses
   Classes,
   SysUtils,
   Laz2_DOM,
   PParser,
   PasTree,
   contnrs,
   Windows,
   CodeCoverage.FileWriter,
   CodeCoverage.GDB;

const
   CodeCoverageFileExtension: string = '.xml';
   CodeCoverageExecutedLinesExtension: string = '.executedLines.lst';
   CodeCoverageTestMethodsExtension: string = '.testMethods.lst';
   CodeCoverageCoveredExtension: string = '.covered.lst';

type
   TGetTestClassNames = procedure(ANames: TStringList) of object;

   TOnParserEngineCreateElement = procedure(AClass: TPTreeElement; const AName: string; AParent: TPasElement; AVisibility: TPasMemberVisibility;
      const ASourceFilename: string; ASourceLinenumber: integer) of object;

   { TCodeCoverage }

   TCodeCoverage = class
   private
      FDocuments: TCodeCoverageFiles;
      FExecutableCountsAsCoverage: boolean;
      FOnUnknownMethodEncountered: TOnUnknownMethodEncounteredEvent;
      FSourceUnits: TStringList;
      FTestCaseUnits: TStringList;
      FClassAssociations: TStringList;
      FFunctionAssociations: TStringList;
      FCoveredThings: TStringList;
      FTestMethods: TStringList;
      {
        Loads the filenames of the testcase and tested source units.
      }
      procedure LoadUnitFilenames;
      {
        ParseSourceByFilename parses a Pascal source file for classes,
        methods und functions.

        @param(AFilename The name of the Pascal source file.)
      }
      procedure ParseSourceByFilename(AFilename: string);
      {
        ParseTestByFilename parses a Pascal testcase file for a list of
        covered methods und functions, either writing it to the XML,
        or a separate list.

        @param(AFilename The name of the Pascal source file.)
        @param(ACoversList If not nil, will be filled with covered methods and functions.)
      }
      procedure ParseTestByFilename(AFilename: string; ACoversList: TStrings);
      {
        ParseSources loops through all specified source files, parsing them
        by calling ParseSourceByFilename on them.
      }
      procedure ParseSources;
      {
        ParseTests loops through all specified testcasesource files, parsing them
        by calling ParseTestByFilename on them.
      }
      procedure ParseTests(ACoversList: TStrings);
      {
        FindCoveredThings finds all covered methods and functions from the
        comments in the testcase units.
      }
      procedure FindCoveredThings;
      procedure SetOnUnknownMethodEncountered(AValue: TOnUnknownMethodEncounteredEvent);
      procedure UpdateClassesCoverage;
   protected
      {
        GetTestClassNames retrieves names of test classes.

        @param(ANames Should be filled with names of classes.)
      }
      procedure GetTestClassNames({%H-}ANames: TStringList); virtual;
      {
        FindTestUnitFilenames finds the full testcase unit filenames
        from the content of the Gnu Debugger file (.gdb).

        @param(AGnuDebuggerFile A class holding information from the .gdb file.)
      }
      procedure FindTestUnitFilenames(AGnuDebuggerFile: TGnuDebuggerFile); virtual;
      {
        FindSourceUnitFilenames finds the full tested unit filenames
        from the content of the Gnu Debugger file (.gdb).

        @param(AGnuDebuggerFile A class holding information from the .gdb file.)
      }
      procedure FindSourceUnitFilenames(AGnuDebuggerFile: TGnuDebuggerFile); virtual;
      procedure InsertLineCoverageFromFile(AFilename: string);
      {
        DoCreateElementInSourceFile is called by the PasTreeContainer and
        required to find all lines in source units.
      }
      procedure DoCreateElementInSourceFile(AClass: TPTreeElement; const AName: string; {%H-}AParent: TPasElement; {%H-}AVisibility: TPasMemberVisibility;
         const ASourceFilename: string; ASourceLinenumber: integer);
      procedure DoTestTestMethodElementInSourceFile(AClass: TPTreeElement; const AName: string; {%H-}AParent: TPasElement; {%H-}AVisibility: TPasMemberVisibility;
         const {%H-}ASourceFilename: string; {%H-}ASourceLinenumber: integer);
   public
      class var TestAssociations: TStringList;
      class constructor Create;
      class destructor Destroy;
   public
      constructor Create;
      destructor Destroy; override;
      {
        PreProcessCodeCoverage prepares code coverage information available
        before running the test.
      }
      procedure PreProcessCodeCoverage;
      {
        PostProcessCodeCoverage takes executed line information and
        combines it with the code coverage report.
      }
      procedure PostProcessCodeCoverage;
      function GetResultsAsConsoleOutput: string;
      property TestCaseUnits: TStringList read FTestCaseUnits;
      property SourceUnits: TStringList read FSourceUnits;
      property TestMethods: TStringList read FTestMethods;
      property Documents: TCodeCoverageFiles read FDocuments;
      property ExecutableCountsAsCoverage: boolean read FExecutableCountsAsCoverage write FExecutableCountsAsCoverage;
      property OnUnknownMethodEncountered: TOnUnknownMethodEncounteredEvent read FOnUnknownMethodEncountered write SetOnUnknownMethodEncountered;
   end;

procedure RegisterCodeCoverageTestAssocation(AClassName, AUnitName: string);

implementation

uses
   testregistry,
   RegExpr;

type

   { TPickupCommentsPasTreeContainer }

   TPickupCommentsPasTreeContainer = class(TPasTreeContainer)
   private
      FOnCreateElement: TOnParserEngineCreateElement;
      FSourceFilename: string;
   public
      function CreateElement(AClass: TPTreeElement; const AName: string; AParent: TPasElement; AVisibility: TPasMemberVisibility; const ASourceFilename: string;
         ASourceLinenumber: integer): TPasElement; override;
      function FindElement(const {%H-}AName: string): TPasElement; override;
      property OnCreateElement: TOnParserEngineCreateElement read FOnCreateElement write FOnCreateElement;
      property SourceFilename: string read FSourceFilename write FSourceFilename;
   end;


function TPickupCommentsPasTreeContainer.CreateElement(AClass: TPTreeElement; const AName: string; AParent: TPasElement; AVisibility: TPasMemberVisibility;
   const ASourceFilename: string; ASourceLinenumber: integer): TPasElement;
begin
   Result := AClass.Create(AName, AParent);
   Result.Visibility := AVisibility;
   Result.SourceFilename := ASourceFilename;
   Result.SourceLinenumber := ASourceLinenumber;
   Result.DocComment := CurrentParser.SavedComments;
   if Assigned(FOnCreateElement) then begin
      FOnCreateElement(AClass, AName, AParent, AVisibility, ASourceFilename, ASourceLinenumber);
   end;
end;

function TPickupCommentsPasTreeContainer.FindElement(const AName: string): TPasElement;
begin
   Result := nil;
end;

procedure RegisterCodeCoverageTestAssocation(AClassName, AUnitName: string);
begin
   TCodeCoverage.TestAssociations.Add(AClassName + '=' + AUnitName);
end;

{ TCodeCoverage }

procedure TCodeCoverage.FindTestUnitFilenames(AGnuDebuggerFile: TGnuDebuggerFile);
var
   slClasses: TStringList;
   iClass: integer;
   sUnitName: string;
begin
   slClasses := TStringList.Create;
   try
      GetTestClassNames(slClasses);
      for iClass := 0 to Pred(slClasses.Count) do begin
         sUnitName := TCodeCoverage.TestAssociations.Values[slClasses[iClass]];
         if AGnuDebuggerFile.GetUnitFullFilename(sUnitName) then begin
            TestCaseUnits.Add(sUnitName);
         end;
      end;
   finally
      slClasses.Free;
   end;
   {$IFDEF DebugDumpCodeCoverageInformationLists}
   TestCaseUnits.SaveToFile(ParamStr(0) + '.testunits.txt');
   {$ENDIF DebugDumpCodeCoverageInformationLists}
   TestMethods.SaveToFile(ParamStr(0) + CodeCoverageTestMethodsExtension);
end;

procedure TCodeCoverage.FindSourceUnitFilenames(AGnuDebuggerFile: TGnuDebuggerFile);
var
   slAllFiles: TStringList;
   iFile: integer;

   procedure FindInUnit(AFilename: string);

      procedure ProcessThing(AThing: string);
      var
         iThing, iFilename: integer;
      begin
         iThing := FCoveredThings.IndexOf(AThing);
         if iThing > -1 then begin
            iFilename := FSourceUnits.IndexOf(AFilename);
            if iFilename < 0 then begin
               FSourceUnits.Add(AFilename);
            end;
         end;
      end;

      procedure ProcessElement(AElement: TPasElement);
      begin
         if AElement is TPasClassType then begin
            ProcessThing(AElement.Name);
         end;
         if AElement is TPasProcedure then begin
            ProcessThing(AElement.Name);
         end;
      end;

   var
      ptc: TPickupCommentsPasTreeContainer;
      pm: TPasModule;
      d: TFPList;
      i: integer;
      pe: TPasElement;
   begin
      if Pos('\lazarus\lcl\', AFilename) > 0 then begin
         Exit;
      end;
      if Pos('\lazarus\components\', AFilename) > 0 then begin
         Exit;
      end;
      OutputDebugString(PChar(Format('Parsing (%d/%d) %s...', [iFile + 1, slAllFiles.Count, slAllFiles[iFile]])));
      try
         ptc := TPickupCommentsPasTreeContainer.Create;
         try
            ptc.SourceFilename := AFilename;
            //ptc.OnCreateElement := @DoCreateElementInSourceFile;
            ptc.NeedComments := False;
            pm := ParseSource(ptc, AFilename, 'win32', 'i386');
            try
               if Assigned(pm.InterfaceSection) then begin
                  d := pm.InterfaceSection.Declarations;
                  for i := 0 to Pred(d.Count) do begin
                     pe := (TObject(d[i]) as TPasElement);
                     ProcessElement(pe);
                  end;
               end;
               if Assigned(pm.ImplementationSection) then begin
                  d := pm.ImplementationSection.Declarations;
                  for i := 0 to Pred(d.Count) do begin
                     pe := (TObject(d[i]) as TPasElement);
                     ProcessElement(pe);
                  end;
               end;
            finally
               pm.Free;
            end;
         finally
            ptc.Free;
         end;
      except
         // broken files can happen
         on E: Exception do begin
            //ShowMessage('Error parsing ' + ExtractFilename(AFilename) + LineEnding + E.Message);
         end;
      end;
   end;

begin
   slAllFiles := TStringList.Create;
   try
      AGnuDebuggerFile.ListUnitFilenames(slAllFiles);
      {$IFDEF DebugDumpCodeCoverageInformationLists}
      slAllFiles.SaveToFile(ParamStr(0) + '.gdbunits.txt');
      {$ENDIF DebugDumpCodeCoverageInformationLists}
      for iFile := 0 to Pred(slAllFiles.Count) do begin
         FindInUnit(slAllFiles[iFile]);
      end;
   finally
      slAllFiles.Free;
   end;
   {$IFDEF DebugDumpCodeCoverageInformationLists}
   SourceUnits.SaveToFile(ParamStr(0) + '.sourceunits.txt');
   {$ENDIF DebugDumpCodeCoverageInformationLists}
end;

procedure TCodeCoverage.InsertLineCoverageFromFile(AFilename: string);
var
   sl: TStringList;
   i: integer;
   doc: TCodeCoverageXML;
begin
   if not FileExists(AFilename) then begin
      Exit;
   end;
   sl := TStringList.Create;
   try
      sl.LoadFromFile(AFilename);
      sl.NameValueSeparator := '=';
      for i := 0 to Pred(sl.Count) do begin
         doc := FDocuments.FindDocumentByFilename(sl.Names[i], true, false);
         if Assigned(doc) then begin
            doc.AddCoverageLineToken(sl.Names[i], StrToIntDef(sl.ValueFromIndex[i], -1));
         end;
      end;
      FDocuments.Save;
   finally
      sl.Free;
   end;
end;

procedure TCodeCoverage.ParseSourceByFilename(AFilename: string);
var
   nFile: TDOMNode;
   doc: TCodeCoverageXML;

   procedure ProcessElement(AElement: TPasElement);
   begin
      if AElement is TPasClassType then begin
         doc.FindClassNode(nFile, AElement.Name, True, AElement.SourceLinenumber);
         if FClassAssociations.IndexOfName(AElement.Name) < 0 then begin
            FClassAssociations.AddObject(AElement.Name + '=' + AFilename, doc);
         end;
      end;
      if AElement is TPasProcedure then begin
         doc.AddMethod(nFile, TPasProcedure(AElement).FullName, AElement.GetDeclaration(True), AElement.SourceLinenumber);
         FFunctionAssociations.AddObject(AElement.Name + '=' + AFilename, doc);
      end;
   end;

var
   ptc: TPickupCommentsPasTreeContainer;
   pm: TPasModule;
   d: TFPList;
   i: integer;
   pe: TPasElement;

begin
   doc := FDocuments.FindDocumentByFilename(AFilename, FExecutableCountsAsCoverage);
   doc.OnUnknownMethodEncountered := FOnUnknownMethodEncountered;
   try
      nFile := doc.FindFileNode(AFilename);
      ptc := TPickupCommentsPasTreeContainer.Create;
      ptc.SourceFilename := AFilename;
      ptc.OnCreateElement := @DoCreateElementInSourceFile;
      try
         ptc.NeedComments := True;
         pm := ParseSource(ptc, AFilename, 'win32', 'i386');
         try
            if Assigned(pm.InterfaceSection) then begin
               d := pm.InterfaceSection.Declarations;
               for i := 0 to Pred(d.Count) do begin
                  pe := (TObject(d[i]) as TPasElement);
                  ProcessElement(pe);
                  //OutputDebugString(PChar(Format('interface %d: %s (%s) - %s', [i, pe.Name, pe.ClassName, pe.GetDeclaration(True)])));
               end;
            end;
            if Assigned(pm.ImplementationSection) then begin
               d := pm.ImplementationSection.Declarations;
               for i := 0 to Pred(d.Count) do begin
                  pe := (TObject(d[i]) as TPasElement);
                  ProcessElement(pe);
                  //OutputDebugString(PChar(Format('implementation %d: %s (%s) - %s', [i, pe.Name, pe.ClassName, pe.GetDeclaration(True)])));
               end;
            end;
         finally
            pm.Free;
         end;
      finally
         ptc.Free;
      end;
   finally
      doc.Save;
   end;
end;

procedure TCodeCoverage.ParseTestByFilename(AFilename: string; ACoversList: TStrings);

   procedure ProcessCoversStatement(AElement: TPasElement; AStatement: string);
   var
      doc: TCodeCoverageXML;
      iXML, iDot: integer;
      sClassName, sMethodName: string;
   begin
      if Assigned(ACoversList) then begin
         ACoversList.Add(AStatement);
         Exit;
      end;
      iDot := Pos('.', AStatement);
      if iDot > 0 then begin
         sClassName := Copy(AStatement, 1, Pred(iDot));
         sMethodName := Copy(AStatement, Succ(iDot));
         iXML := FClassAssociations.IndexOfName(sClassName);
         if iXML > -1 then begin
            doc := TCodeCoverageXML(FClassAssociations.Objects[iXML]);
            doc.RegisterMethodCoveringCode(FClassAssociations.ValueFromIndex[iXML], sClassName, sMethodName, AElement.FullName);
            doc.Save;
         end;
      end else begin
         iXML := FFunctionAssociations.IndexOfName(AStatement);
         if iXML > -1 then begin
            doc := TCodeCoverageXML(FFunctionAssociations.Objects[iXML]);
            doc.RegisterFunctionCoveringCode(FFunctionAssociations.ValueFromIndex[iXML], AStatement, AElement.FullName);
            doc.Save;
         end;
      end;
   end;

   procedure ProcessElement(AElement: TPasElement);
   var
      r: TRegExpr;
   begin
      if AElement is TPasProcedure then begin
         //if SameText('test', Copy(AElement.Name, 1, 4)) or (Pos('.test', LowerCase(AElement.Name)) > 0) then begin
         if (TestMethods.IndexOf(AElement.Name) > -1) then begin
            r := TRegExpr.Create('@covers\(([^\)]*)\)');
            if r.Exec(AElement.DocComment) then begin
               repeat
                  ProcessCoversStatement(AElement, r.Match[1]);
               until not r.ExecNext;
            end;
         end;
      end;
   end;

var
   ptc: TPickupCommentsPasTreeContainer;
   pm: TPasModule;
   d: TFPList;
   i: integer;
   pe: TPasElement;
begin
   ptc := TPickupCommentsPasTreeContainer.Create;
   try
      ptc.NeedComments := True;
      ptc.OnCreateElement := @DoTestTestMethodElementInSourceFile;
      pm := ParseSource(ptc, AFilename, 'win32', 'i386');
      try
         if Assigned(pm.InterfaceSection) then begin
            d := pm.InterfaceSection.Declarations;
            for i := 0 to Pred(d.Count) do begin
               pe := (TObject(d[i]) as TPasElement);
               ProcessElement(pe);
            end;
         end;
         if Assigned(pm.ImplementationSection) then begin
            d := pm.ImplementationSection.Declarations;
            for i := 0 to Pred(d.Count) do begin
               pe := (TObject(d[i]) as TPasElement);
               ProcessElement(pe);
            end;
         end;
      finally
         pm.Free;
      end;
   finally
      ptc.Free;
   end;
end;

constructor TCodeCoverage.Create;
begin
   FTestCaseUnits := TStringList.Create;
   FSourceUnits := TStringList.Create;
   FClassAssociations := TStringList.Create;
   FFunctionAssociations := TStringList.Create;
   FCoveredThings := TStringList.Create;
   FTestMethods := TStringList.Create;
   FTestMethods.Sorted := True;
   FTestMethods.Duplicates := dupIgnore;
   FDocuments := TCodeCoverageFiles.Create(True);
   FExecutableCountsAsCoverage := True;
end;

destructor TCodeCoverage.Destroy;
begin
   FDocuments.Free;
   FTestCaseUnits.Free;
   FSourceUnits.Free;
   FClassAssociations.Free;
   FFunctionAssociations.Free;
   FCoveredThings.Free;
   FTestMethods.Free;
   inherited Destroy;
end;

procedure TCodeCoverage.PreProcessCodeCoverage;
begin
   FDocuments.Clear;
   FClassAssociations.Clear;
   FFunctionAssociations.Clear;
   FTestMethods.Clear;
   SourceUnits.Clear;
   TestCaseUnits.Clear;
   LoadUnitFilenames;
end;

procedure TCodeCoverage.PostProcessCodeCoverage;
begin
   ParseSources;
   ParseTests(nil);
   InsertLineCoverageFromFile(ParamStr(0) + CodeCoverageExecutedLinesExtension);
   UpdateClassesCoverage;
end;

function TCodeCoverage.GetResultsAsConsoleOutput: string;
var
   iDoc: integer;
   ac: TClassNodes;
   eClass: TDOMElement;
   doc: TCodeCoverageXML;
   iMethodsTotal: integer;
   iMethodsTested: integer;
   iMethodsTotalSum: integer;
   iMethodsTestedSum: integer;
   iMethodsCoverageSum: integer;
   iMethodsCoverage: integer;
   sTrailing: string;
   iClassesTotal: integer;
   iClassesTested: integer;
   iClassesCoverage: integer;
begin
   Result := 'Code Coverage Report:' + LineEnding;
   Result := Result + FormatDateTime('  yyyy-mm-dd hh:nn:ss', Now) + LineEnding + LineEnding;
   sTrailing := '';
   iMethodsTotalSum := 0;
   iMethodsTestedSum := 0;
   iClassesTotal := 0;
   iClassesTested := 0;
   for iDoc := 0 to Pred(FDocuments.Count) do begin
      doc := FDocuments.Files[iDoc];
      ac := doc.ListClassNodes(doc.GetPrimaryFileNode);
      for eClass in ac do begin
         if eClass.hasAttribute('name') then begin
            doc.GetClassTotals(eClass, iMethodsTotal, iMethodsTested, iMethodsCoverage);
            Inc(iMethodsTestedSum, iMethodsTested);
            Inc(iMethodsTotalSum, iMethodsTotal);
            sTrailing := sTrailing + eClass.GetAttribute('name') + LineEnding;
            sTrailing := sTrailing + Format('  Methods: %03d%s %-11s', [iMethodsCoverage, '.00%', Format('(%d/%d)', [iMethodsTested, iMethodsTotal])]);
            sTrailing := sTrailing + Format('  Lines: %03d%s %-11s', [0, '.00%', Format('(%d/%d)', [0, 0])]) + LineEnding;
            Inc(iClassesTotal);
            if iMethodsCoverage = 100 then begin
               Inc(iClassesTested);
            end;
         end;
      end;
   end;
   Result := Result + ' Summary:' + LineEnding;
   if iClassesTotal > 0 then begin
      iClassesCoverage := Round(100 * (iClassesTested / iClassesTotal));
   end else begin
      iClassesCoverage := 0;
   end;
   Result := Result + Format('  Classes: %3d%s (%d/%d)', [iClassesCoverage, '.00%', iClassesTested, iClassesTotal]) + LineEnding;
   if iMethodsTotalSum > 0 then begin
      iMethodsCoverageSum := Round(100 * (iMethodsTestedSum / iMethodsTotalSum));
   end else begin
      iMethodsCoverageSum := 0;
   end;
   Result := Result + Format('  Methods: %3d%s %s', [iMethodsCoverageSum, '.00%', Format('(%d/%d)', [iMethodsTestedSum, iMethodsTotalSum])]) + LineEnding;
   Result := Result + Format('  Lines:   %3d%s %s', [0, '.00%', '(0/0)']) + LineEnding;
   Result := Result + LineEnding;
   Result := Result + sTrailing;
end;

procedure TCodeCoverage.LoadUnitFilenames;
var
   dbg: TGnuDebuggerFile;
   sGDBFilename: string;
begin
   dbg := TGnuDebuggerFile.Create;
   try
      // TODO : make sure debug format is stabs
      sGDBFilename := Copy(ParamStr(0), 1, Length(ParamStr(0)) - Length(ExtractFileExt(ParamStr(0)))) + '.dbg';
      dbg.LoadFromFile(sGDBFilename);
      //dbg.StabStrings.SaveToFile(ParamStr(0) + '.debugStrings.lst');
      FindTestUnitFilenames(dbg);
      FindCoveredThings;
      FindSourceUnitFilenames(dbg);
   finally
      dbg.Free;
   end;
end;

procedure TCodeCoverage.FindCoveredThings;
begin
   FCoveredThings.Clear;
   ParseTests(FCoveredThings);
   FCoveredThings.SaveToFile(ParamStr(0) + CodeCoverageCoveredExtension);
end;

procedure TCodeCoverage.SetOnUnknownMethodEncountered(AValue: TOnUnknownMethodEncounteredEvent);
var
   iFile: integer;
begin
   FOnUnknownMethodEncountered := AValue;
   for iFile := 0 to Pred(FDocuments.Count) do begin
      FDocuments.Files[iFile].OnUnknownMethodEncountered := AValue;
   end;
end;

procedure TCodeCoverage.UpdateClassesCoverage;
var
   iFile: integer;
begin
   for iFile := 0 to Pred(FDocuments.Count) do begin
      FDocuments.Files[iFile].UpdateClassesCoverage(FDocuments.Files[iFile].SourceFilename);
   end;
end;

procedure TCodeCoverage.GetTestClassNames(ANames: TStringList);
begin

end;

procedure TCodeCoverage.ParseSources;
var
   i: integer;
begin
   for i := 0 to Pred(FSourceUnits.Count) do begin
      ParseSourceByFilename(FSourceUnits[i]);
   end;
end;

procedure TCodeCoverage.ParseTests(ACoversList: TStrings);
var
   i: integer;
begin
   for i := 0 to Pred(FTestCaseUnits.Count) do begin
      ParseTestByFilename(FTestCaseUnits[i], ACoversList);
   end;
end;

procedure TCodeCoverage.DoCreateElementInSourceFile(AClass: TPTreeElement; const AName: string; AParent: TPasElement; AVisibility: TPasMemberVisibility;
   const ASourceFilename: string; ASourceLinenumber: integer);
var
   doc: TCodeCoverageXML;
begin
   doc := FDocuments.FindDocumentByFilename(ASourceFilename, FExecutableCountsAsCoverage);
   doc.OnUnknownMethodEncountered := FOnUnknownMethodEncountered;
   doc.AddSourceLineToken(ASourceFilename, ASourceLinenumber, AClass.ClassName, AName);
end;

procedure TCodeCoverage.DoTestTestMethodElementInSourceFile(AClass: TPTreeElement; const AName: string; AParent: TPasElement; AVisibility: TPasMemberVisibility;
   const ASourceFilename: string; ASourceLinenumber: integer);
var
   s: string;
begin
   if AClass = TPasProcedure then begin
      if AVisibility = visPublished then begin
         s := AParent.Name + '.' + AName;
         TestMethods.Add(s);
      end;
   end;
end;

class constructor TCodeCoverage.Create;
begin
   TestAssociations := TStringList.Create;
end;

class destructor TCodeCoverage.Destroy;
begin
   TestAssociations.Free;
end;

end.
