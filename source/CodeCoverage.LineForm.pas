{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-21  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit CodeCoverage.LineForm;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   FileUtil,
   SynEdit,
   SynHighlighterPas,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   ExtCtrls,
   StdCtrls,
   Grids,
   CodeCoverage.Graphics,
   CodeCoverage.FileReader;

type

   { TFormLineCoverageInformation }

   TFormLineCoverageInformation = class(TForm)
      bnOk: TButton;
      editCoveredBy: TSynEdit;
      groupDetails: TGroupBox;
      groupCoveredBy: TGroupBox;
      groupSignature: TGroupBox;
      pbCoverage: TPaintBox;
      panelMain: TPanel;
      panelBottom: TPanel;
      editSignature: TSynEdit;
      sgDetails: TStringGrid;
      SynPasSyn1: TSynPasSyn;
      procedure pbCoveragePaint(Sender: TObject);
   private
      FDetails: TLineDetails;
   public
      procedure Execute(const ADetails: TLineDetails);
   end;

procedure ShowLineCoverageInformation(const ADetails: TLineDetails);

implementation

{$R *.lfm}

procedure ShowLineCoverageInformation(const ADetails: TLineDetails);
var
   f: TFormLineCoverageInformation;
begin
   f := TFormLineCoverageInformation.Create(nil);
   try
      f.Execute(ADetails);
   finally
      f.Free;
   end;
end;

function WrapSignatureCode(ASignature: string): string;
begin
   Result := StringReplace(ASignature, '(', '(' + LineEnding + '   ', [rfReplaceAll]);
   Result := StringReplace(Result, ';', ';' + LineEnding + '  ', [rfReplaceAll]);
   Result := StringReplace(Result, ')', LineEnding + '   )', [rfReplaceAll]);
end;

{ TFormLineCoverageInformation }

procedure TFormLineCoverageInformation.pbCoveragePaint(Sender: TObject);
var
   r: TRect;
   iWidth: integer;
   sCoverage: string;
   tsCoverage: TTextStyle;
begin
   r := pbCoverage.Canvas.ClipRect;
   iWidth := r.Right - r.Left;
   r.Right := r.Left + (Round(iWidth * (FDetails.Coverage / 100)));
   GradHorizontal2(pbCoverage.Canvas, r, $0080FF80, False);
   sCoverage := Format('Coverage: %d%s', [FDetails.Coverage, '%']);
   tsCoverage.Alignment := taCenter;
   tsCoverage.Clipping := True;
   tsCoverage.EndEllipsis := True;
   tsCoverage.ExpandTabs := False;
   tsCoverage.Layout := tlCenter;
   tsCoverage.Opaque := False;
   pbCoverage.Canvas.TextRect(pbCoverage.Canvas.ClipRect, 0, 0, sCoverage, tsCoverage);
end;

procedure TFormLineCoverageInformation.Execute(const ADetails: TLineDetails);
begin
   FDetails := ADetails;
   Self.Caption := Format('Line %0:d: %1:s', [ADetails.LineIndex, ADetails.Name]);
   editSignature.Text := WrapSignatureCode(ADetails.Signature);
   editSignature.Height := editSignature.Canvas.TextHeight('X') * Succ(editSignature.Lines.Count);
   editSignature.Visible := Length(ADetails.Signature) > 0;
   editCoveredBy.Text := Trim(ADetails.CoveredBy);
   editCoveredBy.Height := editCoveredBy.Canvas.TextHeight('X') * Succ(editCoveredBy.Lines.Count);
   editCoveredBy.Visible := Length(ADetails.CoveredBy) > 0;
   sgDetails.Cells[0, 0] := 'Executable:';
   sgDetails.Cells[1, 0] := IntToStr(ADetails.Executable);
   sgDetails.Cells[0, 1] := 'Executed:';
   sgDetails.Cells[1, 1] := IntToStr(ADetails.Executed);
   ShowModal;
end;

end.
