unit TestSynEditGutterForm;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   FileUtil,
   SynEdit,
   SynGutter,
   SynHighlighterPas,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   CodeCoverage.SynGutter;

type

   { TForm1 }

   TForm1 = class(TForm)
      SynEdit1: TSynEdit;
      SynPasSyn1: TSynPasSyn;
      procedure FormShow(Sender: TObject);
   private
      FGutter: TSynGutterCodeCoverage;
   public

   end;

var
   Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormShow(Sender: TObject);
begin
   SynEdit1.Lines.LoadFromFile('..\..\source\PepiMK.CodeCoverage.pas');
   FGutter := TSynGutterCodeCoverage.Create(SynEdit1.Gutter.Parts);
   FGutter.Name := 'SynGutterCodeCoverage1';
   with TSynGutterSeparator.Create(SynEdit1.Gutter.Parts) do begin
     Name := 'SynGutterSeparatorCC';
   end;
end;

end.
