{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit CodeCoveragePackage;

{$warn 5023 off : no warning about unused units}
interface

uses
  CodeCoverage.IDEIntegration, CodeCoverage.LineForm, CodeCoverage.SynGutter, 
  LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('CodeCoverage.IDEIntegration', 
    @CodeCoverage.IDEIntegration.Register);
end;

initialization
  RegisterPackage('CodeCoveragePackage', @Register);
end.
