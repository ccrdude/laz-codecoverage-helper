{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-23  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit HelloWorld.Greetings.TestCase;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   {$IF DEFINED(FPCUnit)}
   fpcunit,
   testregistry,
   {$ELSE}
   TestFramework,
   {$IFEND}
   HelloWorld.Greetings;

type

   { THelloWorldTestCase }

   THelloWorldTestCase = class(TTestCase)
   published
      procedure TestEnglish;
      procedure TestPolish;
      procedure TestRomanian;
   end;

implementation

{
  @covers(SayItEnglish)
}
procedure THelloWorldTestCase.TestEnglish;
begin
   CheckEquals('Hello World', SayItEnglish);
end;

{
  @covers(TSayItForeign.SayItPolish)
}
procedure THelloWorldTestCase.TestPolish;
begin
   CheckEquals('Witaj świecie', TSayItForeign.SayItPolish);
end;

{
  @covers(TSayItForeign.SayItRomanian)
}
procedure THelloWorldTestCase.TestRomanian;
begin
   with TSayItForeign.Create do begin
      CheckEquals('Salut Lume', SayItRomanian);
   end;
end;

initialization
   {$IF DEFINED(FPCUnit)}
   RegisterTest(THelloWorldTestCase);
   {$ELSE}
   RegisterTest(THelloWorldTestCase.Suite);
   {$IFEND}
end.
