{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Contains all localizable strings used by the package)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-23  pk  ---  Started moving strings here.
// *****************************************************************************
   )
}

unit CodeCoverage.Strings;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils;

resourcestring
   rsCodeCoverageMenuItemRun = 'Run Code Coverage Test';
   rsCodeCoverageStatusMessageDebuggerName = 'Code Coverage Run using debugger: %s';
   rsCodeCoverageStatusMessageInitFailed = 'Initializing the debugger failed: %s';
   rsCodeCoverageExceptionNoDebuggerExecutableSet = 'Please set DebuggerExecuta'
     +'ble first!';

implementation

end.
