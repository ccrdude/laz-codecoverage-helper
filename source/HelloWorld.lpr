{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-23  --  ---  Renamed from ram HelloWorld to HelloWorld
// 2017-06-23  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

program HelloWorld;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}
{$CODEPAGE UTF8}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
   cthreads, {$ENDIF} {$ENDIF}
   Classes,
   HelloWorld.Greetings { you can add units after this };

begin
   WriteLn(SayItEnglish);
   WriteLn(SayItGerman);
   WriteLn(SayItDutch());
   with TSayItForeign.Create do begin
      WriteLn(SayItRomanian);
      Free;
   end;
   WriteLn(TSayItForeign.SayItPolish);
end.
