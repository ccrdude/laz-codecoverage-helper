{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-19  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit CodeCoverage.UnitFrame;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   FileUtil,
   Forms,
   Controls,
   ComCtrls,
   ExtCtrls,
   Laz2_DOM,
   laz2_XMLRead,
   SynEdit,
   SynGutter,
   SynHighlighterPas,
   //SynEditMarkupSpecialLine,
   Graphics,
   CodeCoverage,
   CodeCoverage.SynGutter,
   CodeCoverage.Graphics,
   CodeCoverage.FileReader;

type

   { TFrameCodeCoverage }

   TFrameCodeCoverage = class(TFrame)
      ilCodeCoverage: TImageList;
      Splitter1: TSplitter;
      editSource: TSynEdit;
      SynPasSyn1: TSynPasSyn;
      tvUnitCoverage: TTreeView;
      procedure editSourceSpecialLineColors(Sender: TObject; Line: integer; var Special: boolean; var {%H-}FG, BG: TColor);
      procedure tvUnitCoverageAdvancedCustomDrawItem(ASender: TCustomTreeView; ANode: TTreeNode; {%H-}State: TCustomDrawState; AStage: TCustomDrawStage;
         var {%H-}PaintImages, {%H-}DefaultDraw: boolean);
      procedure tvUnitCoverageSelectionChanged(Sender: TObject);
   private
      FReader: TCodeCoverageXMLReader;
      FGutter: TSynGutterCodeCoverage;
      FImageIndexUnit: integer;
      FImageIndexClass: integer;
      FImageIndexInterface: integer;
      FImageIndexProcedure: integer;
      FImageIndexFunction: integer;
      {
         DocumentToTreeview displays a code coverage XML document within
         a treeview.
      }
      procedure DocumentToTreeview(ADocument: TXMLDocument; ATreeView: TTreeView);
      procedure InitImageList;
   public
      constructor Create(TheOwner: TComponent); override;
      destructor Destroy; override;
      procedure ShowUnitCodeCoverage(AUnitFilename: string);
   end;

implementation

uses
   Windows;

{$R *.lfm}

{ TFrameCodeCoverage }

procedure TFrameCodeCoverage.tvUnitCoverageAdvancedCustomDrawItem(ASender: TCustomTreeView; ANode: TTreeNode; State: TCustomDrawState; AStage: TCustomDrawStage;
   var PaintImages, DefaultDraw: boolean);

var
   r, rText, rTemp, rPercentage: TRect;
   iWidth: integer;
   pdata: PCodeCoverageNodeData;
begin
   pdata := PCodeCoverageNodeData(ANode.Data);
   r := ANode.DisplayRect(False);
   rText := ANode.DisplayRect(True);
   rTemp := r;
   rTemp.Left := rText.Left;
   iWidth := rTemp.Right - rTemp.Left;
   rPercentage := rTemp;
   rPercentage.Right := rPercentage.Left + Round(iWidth * pdata^.Coverage / 100);
   ASender.Color := clWindow;
   case AStage of
      cdPrePaint:
      begin
         if (rPercentage.Right > rPercentage.Left + 2) and (pdata^.NodeType in [cctntClass, cctntFunction, cctntMethod]) then begin
            ASender.Color := clNone;
            ASender.Canvas.Brush.Color := clWindow;
            ASender.Canvas.FillRect(r);
            GradHorizontal2(ASender.Canvas, rPercentage, $0080FF80, False);
         end;
      end;
   end;
end;

procedure TFrameCodeCoverage.tvUnitCoverageSelectionChanged(Sender: TObject);
var
   pData: PCodeCoverageNodeData;
begin
   if not Assigned(tvUnitCoverage.Selected) then begin
      Exit;
   end;
   pData := PCodeCoverageNodeData(tvUnitCoverage.Selected.Data);
   editSource.CaretY := pData^.LineStart;
   editSource.CaretX := 1;
   editSource.TopLine := pData^.LineStart - 5;
end;

procedure TFrameCodeCoverage.editSourceSpecialLineColors(Sender: TObject; Line: integer; var Special: boolean; var FG, BG: TColor);
var
   iCoverage: integer;
begin
   if Line >= FReader.LineCount then begin
      Exit;
   end;
   iCoverage := FReader.LineCoverage[Line];
   Special := True;
   if iCoverage = 100 then begin
      BG := $0080FF80;
   end else if iCoverage > -1 then begin
      BG := $008080FF;
   end;
end;

procedure TFrameCodeCoverage.DocumentToTreeview(ADocument: TXMLDocument; ATreeView: TTreeView);

   function GetNodeAttributesAsString(ANode: TDOMNode): string;
   var
      i: integer;
   begin
      Result := '';
      if ANode.HasAttributes then begin
         for i := 0 to Pred(ANode.Attributes.Length) do begin
            with ANode.Attributes[i] do begin
               Result := Result + format(' %s="%s"', [NodeName, NodeValue]);
            end;
         end;
      end;
      Result := Trim(Result);
   end;

   procedure ParseXML(ANode: TDOMNode; ATreeNode: TTreeNode);
   var
      sNodeName: string;
      pdata: PCodeCoverageNodeData;
   begin
      if ANode = nil then begin
         Exit;
      end;
      sNodeName := ANode.NodeName;
      if (sNodeName = 'source') and Assigned(ANode.ParentNode) and (ANode.ParentNode.NodeName = 'file') then begin
         Exit;
      end;
      if (sNodeName = 'totals') and Assigned(ANode.ParentNode) and (ANode.ParentNode.NodeName = 'file') then begin
         Exit;
      end;
      New(pdata);
      pData^.ShortName := sNodeName;
      pData^.Declaration := '';
      pData^.NodeType := cctntNone;
      pData^.Coverage := 0;
      pData^.LineStart := 0;
      if ANode is TDOMElement then begin
         if TDOMElement(ANode).hasAttribute('coverage') then begin
            pData^.Coverage := StrToIntDef(TDOMElement(ANode).GetAttribute('coverage'), 0);
         end;
         if TDOMElement(ANode).hasAttribute('start') then begin
            pData^.LineStart := StrToIntDef(TDOMElement(ANode).GetAttribute('start'), 0);
         end;
      end;
      ATreeNode := ATreeView.Items.AddChildObject(ATreeNode, Trim(ANode.NodeName + ' ' + GetNodeAttributesAsString(ANode) + ANode.NodeValue), pdata);
      case sNodeName of
         'file':
         begin
            ATreeNode.Text := TDOMElement(ANode).GetAttribute('path') + TDOMElement(ANode).GetAttribute('name');
            ATreeNode.ImageIndex := FImageIndexUnit;
         end;
         'class':
         begin
            ATreeNode.Text := Format('(%d%s) %s', [pdata^.Coverage, '%', TDOMElement(ANode).GetAttribute('name')]);
            ATreeNode.ImageIndex := FImageIndexClass;
            pdata^.NodeType := cctntClass;
         end;
         'function':
         begin
            ATreeNode.Text := Format('(%d%s) %s', [pdata^.Coverage, '%', TDOMElement(ANode).GetAttribute('signature')]);
            ATreeNode.ImageIndex := FImageIndexProcedure;
            pData^.Declaration := TDOMElement(ANode).GetAttribute('signature');
            pData^.NodeType := cctntFunction;
         end;
         'method':
         begin
            ATreeNode.Text := Format('(%d%s) %s', [pdata^.Coverage, '%', TDOMElement(ANode).GetAttribute('signature')]);
            ATreeNode.ImageIndex := FImageIndexProcedure;
            pData^.Declaration := TDOMElement(ANode).GetAttribute('signature');
            pData^.NodeType := cctntMethod;
         end;
      end;
      ATreeNode.SelectedIndex := ATreeNode.ImageIndex;
      ANode := ANode.FirstChild;
      while ANode <> nil do begin
         ParseXML(ANode, ATreeNode);
         ANode := ANode.NextSibling;
      end;
      case sNodeName of
         'file', 'totals': ATreeNode.Expand(False);
         'class': ATreeNode.Expand(True);
      end;
   end;

begin
   ATreeView.Items.Clear;
   if ADocument.DocumentElement.ChildNodes.Count > 0 then begin
      ParseXML(ADocument.DocumentElement.ChildNodes[0], nil);
   end else begin
      ParseXML(ADocument.DocumentElement, nil);
   end;
end;

procedure TFrameCodeCoverage.InitImageList;
begin
   if ilCodeCoverage.Count = 0 then begin
      FImageIndexUnit := ilCodeCoverage.AddResourceName(HInstance, 'ccce_unit');
      FImageIndexClass := ilCodeCoverage.AddResourceName(HInstance, 'ccce_class');
      FImageIndexProcedure := ilCodeCoverage.AddResourceName(HInstance, 'ccce_procedure');
      FImageIndexFunction := ilCodeCoverage.AddResourceName(HInstance, 'ccce_function');
      FImageIndexInterface := ilCodeCoverage.AddResourceName(HInstance, 'ccce_interface');
   end;
end;

constructor TFrameCodeCoverage.Create(TheOwner: TComponent);
begin
   inherited Create(TheOwner);
   FReader := TCodeCoverageXMLReader.Create;
   FGutter := TSynGutterCodeCoverage.Create(editSource.RightGutter.Parts);
   FGutter.Name := 'SynGutterCodeCoverage1';
   FGutter.CCReader := FReader;
   FGutter.Index := 0;
   with TSynGutterSeparator.Create(editSource.RightGutter.Parts) do begin
      Name := 'SynGutterSeparatorCC';
      Index := 0;
   end;
end;

destructor TFrameCodeCoverage.Destroy;
begin
   FReader.Free;
   inherited Destroy;
end;

procedure TFrameCodeCoverage.ShowUnitCodeCoverage(AUnitFilename: string);
begin
   editSource.Enabled := FileExists(AUnitFilename);
   if editSource.Enabled then begin
      editSource.Lines.LoadFromFile(AUnitFilename);
   end else begin
      editSource.Lines.Clear;
   end;
   FReader.AssignLines(editSource.Lines);
   InitImageList;
   tvUnitCoverage.Enabled := FileExists(AUnitFilename + CodeCoverageFileExtension);
   if tvUnitCoverage.Enabled then begin
      FReader.LoadFromFile(AUnitFilename + CodeCoverageFileExtension);
      // TODO : verify file hash
      DocumentToTreeview(FReader.XMLDocument, tvUnitCoverage);
   end else begin
      tvUnitCoverage.Items.Clear;
   end;
end;

end.
