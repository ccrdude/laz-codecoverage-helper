unit CodeCoverage.FPCUnit.TestCase;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   fpcunit,
   testregistry,
   CodeCoverage.FPCUnit,
   CodeCoverage;

type

   TCodeCoverageFPCUnitTestCase = class(TTestCase)
   published
      procedure TestFooBar;
   end;

implementation

(**
* @covers(RegisterCodeCoverageTestAssocation)
* @covers(TCodeCoverage.LoadUnitFilenames)
* @covers(TCodeCoverage.ParseSourceByFilename)
* @covers(TCodeCoverage.ParseTestByFilename)
* @covers(TCodeCoverage.ParseSources)
* @covers(TCodeCoverage.ParseTests)
* @covers(TCodeCoverage.FindCoveredThings)
* @covers(TCodeCoverage.UpdateClassesCoverage)
* @covers(TCodeCoverage.GetTestClassNames)
* @covers(TCodeCoverage.FindTestUnitFilenames)
* @covers(TCodeCoverage.FindSourceUnitFilenames)
* @covers(TCodeCoverage.DoCreateElementInSourceFile)
* @covers(TCodeCoverage.Create)
* @covers(TCodeCoverage.Destroy)
* @covers(TCodeCoverage.PreProcessCodeCoverage)
* @covers(TCodeCoverage.PostProcessCodeCoverage)
* @covers(TCodeCoverageFPCUnit.GetTestClassNames)
* @covers(TCodeCoverageFPCUnit.FindTestUnitFilenames)
 *)
procedure TCodeCoverageFPCUnitTestCase.TestFooBar;
var
   c: TCodeCoverage;
begin
   c := TCodeCoverageFPCUnit.Create;
   try
      c.PreProcessCodeCoverage;
      c.PostProcessCodeCoverage;
      CheckTrue(c.TestCaseUnits.Count > 0, 'TCodeCoverageFPCUnit.TestCaseUnits.Count');
      CheckTrue(c.SourceUnits.Count > 0, 'TCodeCoverageFPCUnit.SourceUnits.Count');
   finally
      c.Free;
   end;
end;


initialization

   {$IFDEF FPCUnit}
   RegisterTest(TCodeCoverageFPCUnitTestCase);
   {$ENDIF FPCUnit}
   {$IFDEF FPTest}
   RegisterCodeCoverageTest(TCodeCoverageFPCUnitTestCase);
   {$ENDIF FPTest}

end.
