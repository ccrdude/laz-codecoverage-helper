        {
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-18  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit CodeCoverage.FPCUnit.ConsoleTestRunner;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   fpcunit,
   consoletestrunner,
   CodeCoverage,
   CodeCoverage.FPCUnit;

type

   { TCodeCoverageTestRunner }

   TCodeCoverageTestRunner = class(TTestRunner)
   protected
      FCodeCoverage: TCodeCoverage;
      procedure DoTestRun(ATest: TTest); override;
   public
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
   end;


implementation

//uses
//   Process;

{ TCodeCoverageTestRunner }

procedure TCodeCoverageTestRunner.DoTestRun(ATest: TTest);
//var
//   s: string;
begin
   FCodeCoverage.PreProcessCodeCoverage;
   inherited DoTestRun(ATest);
   FCodeCoverage.PostProcessCodeCoverage;
   WriteLn(FCodeCoverage.GetResultsAsConsoleOutput);
   //if FindCmdLineSwitch('ccbrowser') and (FCodeCoverage.SourceUnits.Count > 0) then begin
   //   RunCommand(ExtractFilePath(ParamStr(0)) + 'CCBrowser.exe', [ExtractFilePath(FCodeCoverage.SourceUnits[0])], s);
   //end;
end;

constructor TCodeCoverageTestRunner.Create(AOwner: TComponent);
begin
   inherited Create(AOwner);
   FCodeCoverage := TCodeCoverageFPCUnit.Create;
end;

destructor TCodeCoverageTestRunner.Destroy;
begin
   FCodeCoverage.Free;
   inherited Destroy;
end;

end.
