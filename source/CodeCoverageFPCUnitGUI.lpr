{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Code Coverage UI test case for FPCUnit.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-13  --  ---  Renamed from ram PepiMKCodeCoverageTestsFPUI to PepiMKCodeCoverageTestsFPUI
// 2017-06-13  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

program CodeCoverageFPCUnitGUI;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

uses
   Interfaces,
   Forms,
   GuiTestRunner,
   CodeCoverage.FPCUnit.GuiTestRunner,
   CodeCoverage.TestCase,
   CodeCoverage.FPCUnit.TestCase,
   CodeCoverage.GDB.TestCase;

{$R *.res}

begin
   Application.Initialize;
   Application.CreateForm(TGUITestRunner, TestRunner);
   Application.Run;
end.
