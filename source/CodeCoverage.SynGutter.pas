unit CodeCoverage.SynGutter;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   Graphics,
   Windows,
   SynEdit,
   SynTextDrawer,
   SynGutterBase,
   SynEditMouseCmds,
   CodeCoverage.FileReader,
   CodeCoverage.LineForm;

type

   { TSynGutterCodeCoverage }

   TSynGutterCodeCoverage = class(TSynGutterPartBase)
   private
      FCCReader: TCodeCoverageXMLReader;
      FTextDrawer: TheTextDrawer;
   protected
      procedure Init; override;
      function PreferedWidth: integer; override;
   public
      procedure Paint(Canvas: TCanvas; AClip: TRect; FirstLine, LastLine: integer); override;
      function DoHandleMouseAction(AnAction: TSynEditMouseAction; var AnInfo: TSynEditMouseActionInfo): boolean; override;
      function MaybeHandleMouseAction(var AnInfo: TSynEditMouseActionInfo; HandleActionProc: TSynEditMouseActionHandler): boolean; override;
      property CCReader: TCodeCoverageXMLReader read FCCReader write FCCReader;
   end;

implementation

uses
   Dialogs,
   LCLIntf;

{ TSynGutterCodeCoverage }

procedure TSynGutterCodeCoverage.Init;
begin
   inherited Init;
   FTextDrawer := Gutter.TextDrawer;
end;

function TSynGutterCodeCoverage.PreferedWidth: integer;
begin
   Result := 4 * (FTextDrawer.CharWidth + 1);
end;

procedure TSynGutterCodeCoverage.Paint(Canvas: TCanvas; AClip: TRect; FirstLine, LastLine: integer);
var
   i, c, iLine: integer;
   iCoverage: integer;
   rcLine: TRect;
   s: string;
   dc: HDC;
   iLineHeight: integer;
begin
   if not Visible then begin
      Exit;
   end;
   iLineHeight := TCustomSynEdit(SynEdit).LineHeight;
   c := TCustomSynEdit(SynEdit).Lines.Count;
   // Changed to use fTextDrawer.BeginDrawing and fTextDrawer.EndDrawing only
   // when absolutely necessary.  Note: Never change brush / pen / font of the
   // canvas inside of this block (only through methods of fTextDrawer)!
   if MarkupInfo.Background <> clNone then begin
      Canvas.Brush.Color := MarkupInfo.Background;
   end else begin
      Canvas.Brush.Color := Gutter.Color;
   end;
   dc := Canvas.Handle;
   LCLIntf.SetBkColor(dc, TColorRef(Canvas.Brush.Color));
   FTextDrawer.BeginDrawing(dc);
   try
      if MarkupInfo.Background <> clNone then begin
         FTextDrawer.SetBackColor(MarkupInfo.Background);
      end else begin
         FTextDrawer.SetBackColor(Gutter.Color);
      end;
      if MarkupInfo.Foreground <> clNone then begin
         fTextDrawer.SetForeColor(MarkupInfo.Foreground);
      end else begin
         fTextDrawer.SetForeColor(TCustomSynEdit(SynEdit).Font.Color);
      end;
      fTextDrawer.SetFrameColor(MarkupInfo.FrameColor);
      fTextDrawer.Style := MarkupInfo.Style;
      // prepare the rect initially
      rcLine := AClip;
      rcLine.Bottom := AClip.Top;
      for i := FirstLine to LastLine do begin
         iLine := FoldView.DisplayNumber[i];
         if (iLine < 0) or (iLine > c) then begin
            break;
         end;
         // next line rect
         rcLine.Top := rcLine.Bottom;
         s := '    ';
         FTextDrawer.SetBackColor(Gutter.Color);
         FTextDrawer.SetForeColor(SynEdit.Font.Color);
         if Assigned(FCCReader) then begin
            if iLine < FCCReader.LineCount then begin
               iCoverage := FCCReader.LineCoverage[iLine];
               if iCoverage > -1 then begin
                  s := Format('%3d', [iCoverage]) + '%';
                  if iCoverage = 100 then begin
                     FTextDrawer.SetBackColor($0080FF80);
                     FTextDrawer.SetForeColor(clBlack);
                  end else if iCoverage > 0 then begin
                     FTextDrawer.SetBackColor($0080FFFF);
                     FTextDrawer.SetForeColor(clBlack);
                  end else if iCoverage = 0 then begin
                     FTextDrawer.SetBackColor($008080FF);
                     FTextDrawer.SetForeColor(clBlack);
                  end;
               end;
            end;
         end;
         Inc(rcLine.Bottom, iLineHeight);
         // erase the background and draw the line number string in one go
         fTextDrawer.ExtTextOut(rcLine.Left, rcLine.Top, ETO_OPAQUE or ETO_CLIPPED, rcLine,
            PChar(Pointer(S)), Length(S));
      end;
      // now erase the remaining area if any
      if AClip.Bottom > rcLine.Bottom then begin
         rcLine.Top := rcLine.Bottom;
         rcLine.Bottom := AClip.Bottom;
         with rcLine do begin
            fTextDrawer.ExtTextOut(Left, Top, ETO_OPAQUE, rcLine, nil, 0);
         end;
      end;
   finally
      fTextDrawer.EndDrawing;
   end;
end;

function TSynGutterCodeCoverage.DoHandleMouseAction(AnAction: TSynEditMouseAction; var AnInfo: TSynEditMouseActionInfo): boolean;
var
   iCoverage: integer;
begin
   if AnAction = nil then
      exit;
   if (AnAction.Command = emcNone) then
      exit;
   //Result := inherited DoHandleMouseAction(AnAction, AnInfo);
   if Assigned(FCCReader) then begin
      if AnInfo.NewCaret.OldLinePos < FCCReader.LineCount then begin
         iCoverage := FCCReader.LineCoverage[AnInfo.NewCaret.OldLinePos];
         if iCoverage > -1 then begin
            ShowLineCoverageInformation(FCCReader.LineDetails[AnInfo.NewCaret.OldLinePos]);
         end;
      end;
   end;
end;

function TSynGutterCodeCoverage.MaybeHandleMouseAction(var AnInfo: TSynEditMouseActionInfo; HandleActionProc: TSynEditMouseActionHandler): boolean;
begin
   Result := inherited MaybeHandleMouseAction(AnInfo, HandleActionProc);

end;


end.
