{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-21  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit CodeCoverage.FileReader;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Laz2_DOM,
   laz2_XMLRead;

type
   TCodeCoverageNodeType = (cctntNone, cctntClass, cctntMethod, cctntFunction);

   TCodeCoverageNodeData = record
      ShortName: string;
      Declaration: string;
      LineStart: integer;
      Coverage: integer;
      NodeType: TCodeCoverageNodeType;
   end;
   PCodeCoverageNodeData = ^TCodeCoverageNodeData;

   TLineDetails = record
      LineIndex: integer;
      Coverage: integer;
      Executed: integer;
      Executable: integer;
      Name: string;
      Signature: string;
      CoveredBy: string;
   end;

   { TCodeCoverageXMLReader }

   TCodeCoverageXMLReader = class
   private
      FXMLDocument: TXMLDocument;
      FLineData: array of integer;
      FLineInformation: array of string;
      FLineDetails: array of TLineDetails;
      FLines: TStringList;
      function GetLineCount: integer;
      function GetLineCoverage(ALineIndex: integer): integer;
      function GetLineDetails(ALineIndex: integer): TLineDetails;
      function GetLineInformation(ALineIndex: integer): string;
      procedure ParseTree;
   public
      constructor Create;
      destructor Destroy; override;
      property XMLDocument: TXMLDocument read FXMLDocument;
      procedure AssignLines(ALines: TStrings);
      procedure LoadFromFile(AFilename: string);
      property LineCount: integer read GetLineCount;
      property LineCoverage[ALineIndex: integer]: integer read GetLineCoverage;
      property LineInformation[ALineIndex: integer]: string read GetLineInformation;
      property LineDetails[ALineIndex: integer]: TLineDetails read GetLineDetails;
   end;


implementation

uses
   Dialogs;

{ TCodeCoverageXMLReader }

procedure TCodeCoverageXMLReader.ParseTree;

   procedure ParseXML(ANode: TDOMNode);
   var
      iLineStart, iCoverage, iCovered, iExecuted, iExecutable: integer;
      sName, sSignature, sCoveredBy: string;
      nCovered: TDOMElement;
   begin
      if ANode = nil then begin
         Exit;
      end;
      if ANode is TDOMElement then begin
         if TDOMElement(ANode).hasAttribute('name') // the tested element
            and TDOMElement(ANode).hasAttribute('coverage') // coverage in %
            and TDOMElement(ANode).hasAttribute('start') then begin // first line
            iLineStart := StrToIntDef(TDOMElement(ANode).GetAttribute('start'), 0);
            iCoverage := StrToIntDef(TDOMElement(ANode).GetAttribute('coverage'), 0);
            sName := TDOMElement(ANode).GetAttribute('name');
            if (iLineStart > 0) and (iLineStart < Length(FLineData)) then begin
               FLineData[iLineStart] := iCoverage;
               FLineInformation[iLineStart] := Format('Line %d: %s %d%s', [iLineStart, sName, iCoverage, '%']) + LineEnding;
               FLineDetails[iLineStart].LineIndex := iLineStart;
               FLineDetails[iLineStart].Coverage := iCoverage;
               FLineDetails[iLineStart].Name := sName;
               if TDOMElement(ANode).hasAttribute('signature') then begin
                  sSignature := TDOMElement(ANode).GetAttribute('signature');
                  FLineInformation[iLineStart] := FLineInformation[iLineStart] + LineEnding + sSignature + LineEnding + LineEnding;
                  FLineDetails[iLineStart].Signature := sSignature;
               end;
               if TDOMElement(ANode).hasAttribute('executable') then begin
                  iExecutable := StrToIntDef(TDOMElement(ANode).GetAttribute('executable'), 0);
                  FLineInformation[iLineStart] := FLineInformation[iLineStart] + Format('Executable: %d', [iExecutable]) + LineEnding;
                  FLineDetails[iLineStart].Executable := iExecutable;
               end;
               if TDOMElement(ANode).hasAttribute('executed') then begin
                  iExecuted := StrToIntDef(TDOMElement(ANode).GetAttribute('executed'), 0);
                  FLineInformation[iLineStart] := FLineInformation[iLineStart] + Format('Executed: %d', [iExecuted]) + LineEnding;
                  FLineDetails[iLineStart].Executed := iExecuted;
               end;
               FLineInformation[iLineStart] := FLineInformation[iLineStart] + LineEnding;
               for iCovered := 0 to Pred(ANode.ChildNodes.Count) do begin
                  nCovered := TDOMElement(ANode.ChildNodes[iCovered]);
                  if nCovered.NodeName = 'covered' then begin
                     if nCovered.hasAttribute('by') then begin
                        sCoveredBy := nCovered.GetAttribute('by');
                        FLineDetails[iLineStart].CoveredBy := FLineDetails[iLineStart].CoveredBy + sCoveredBy + LineEnding;
                        FLineInformation[iLineStart] := FLineInformation[iLineStart] + Format('Covered by: %s', [sCoveredBy]) + LineEnding;
                     end;
                  end;
               end;
            end;
         end;
         if (ANode.NodeName = 'line') and (ANode.ParentNode.NodeName = 'coverage') then begin
            iLineStart := StrToIntDef(TDOMElement(ANode).GetAttribute('nr'), -1);
            if iLineStart > -1 then begin
               FLineData[iLineStart] := 100;
               FLineDetails[iLineStart].LineIndex:=iLineStart;
               FLineDetails[iLineStart].Coverage:=100;
            end;
         end else if (ANode.NodeName = 'covered') and (ANode.ParentNode.NodeName = 'line') then begin
            iLineStart := StrToIntDef(TDOMElement(ANode.ParentNode).GetAttribute('nr'), -1);
            if (iLineStart > -1) then begin
               if TDOMElement(ANode).hasAttribute('by') then begin
                  FLineDetails[iLineStart].CoveredBy:= FLineDetails[iLineStart].CoveredBy + TDOMElement(ANode).GetAttribute('by') + LineEnding;
               end;
            end;
         end;
      end;
      ANode := ANode.FirstChild;
      while ANode <> nil do begin
         ParseXML(ANode);
         ANode := ANode.NextSibling;
      end;
   end;

begin
   if FXMLDocument.DocumentElement.ChildNodes.Count > 0 then begin
      ParseXML(FXMLDocument.DocumentElement.ChildNodes[0]);
   end else begin
      ParseXML(FXMLDocument.DocumentElement);
   end;
end;

function TCodeCoverageXMLReader.GetLineCount: integer;
begin
   Result := Length(FLineData);
end;

function TCodeCoverageXMLReader.GetLineCoverage(ALineIndex: integer): integer;
begin
   Result := FLineData[ALineIndex];
end;

function TCodeCoverageXMLReader.GetLineDetails(ALineIndex: integer): TLineDetails;
begin
   Result := FLineDetails[ALineIndex];
end;

function TCodeCoverageXMLReader.GetLineInformation(ALineIndex: integer): string;
begin
   Result := FLineInformation[ALineIndex];
end;

constructor TCodeCoverageXMLReader.Create;
begin
   FLines := TStringList.Create;
   SetLength(FLineData, 0);
   SetLength(FLineInformation, 0);
   SetLength(FLineDetails, 0);
end;

destructor TCodeCoverageXMLReader.Destroy;
begin
   FLines.Free;
   FXMLDocument.Free;
   inherited Destroy;
end;

procedure TCodeCoverageXMLReader.AssignLines(ALines: TStrings);
var
   i: integer;
begin
   FLines.Assign(ALines);
   SetLength(FLineData, FLines.Count);
   SetLength(FLineInformation, FLines.Count);
   SetLength(FLineDetails, FLines.Count);
   for i := Low(FLineData) to High(FLineData) do begin
      FLineData[i] := -1;
      FLineInformation[i] := Format('Line %d:', [i]);
      FLineDetails[i].Coverage := -1;
      FLineDetails[i].Executable := 0;
      FLineDetails[i].Executed := 0;
      FLineDetails[i].Name := '';
      FLineDetails[i].Signature := '';
      FLineDetails[i].CoveredBy := '';
   end;
end;

procedure TCodeCoverageXMLReader.LoadFromFile(AFilename: string);
begin
   FXMLDocument.Free;
   if FileExists(AFilename) then begin
      ReadXMLFile(FXMLDocument, AFilename);
      ParseTree;
   end;
end;

end.
