{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Supplying graphical elements for Code Coverage displays.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-21  pk  ---  Moved into separate unit (from CodeCoverage.UnitFrame).
// *****************************************************************************
   )
}

unit CodeCoverage.Graphics;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Math,
   Windows,
   Graphics;

procedure GradHorizontal(Canvas: TCanvas; Rect: TRect; FromColor, ToColor: TColor);
procedure GradHorizontal2(Canvas: TCanvas; Rect: TRect; AColor: TColor; AReverse: boolean);

implementation

procedure GradHorizontal(Canvas: TCanvas; Rect: TRect; FromColor, ToColor: TColor);
var
   X: integer;
   dr, dg, db: extended;
   C1, C2: TColor;
   r1, r2, g1, g2, b1, b2: byte;
   r, G, b: byte;
   iPixelCount: integer;
begin
   if Rect.Right = Rect.Left then begin
      Exit;
   end;
   C1 := FromColor;
   C2 := ToColor;
   r1 := GetRValue(C1);
   g1 := GetGValue(C1);
   b1 := GetBValue(C1);
   r2 := GetRValue(C2);
   g2 := GetGValue(C2);
   b2 := GetBValue(C2);
   dr := (r2 - r1) / (Rect.Right - Rect.Left);
   dg := (g2 - g1) / (Rect.Right - Rect.Left);
   db := (b2 - b1) / (Rect.Right - Rect.Left);
   iPixelCount := 0;
   Canvas.Pen.Style := psSolid;
   for X := Rect.Left to Pred(Rect.Right) do begin
      r := r1 + Ceil(dr * iPixelCount);
      G := g1 + Ceil(dg * iPixelCount);
      b := b1 + Ceil(db * iPixelCount);
      Canvas.Pen.Color := RGB(r, G, b);
      Canvas.MoveTo(X, Rect.Top);
      Canvas.LineTo(X, Rect.Bottom);
      Inc(iPixelCount);
   end;
end;

function ReduceToByteMax(AByte: integer): byte;
begin
   if AByte > 255 then begin
      Result := 255;
   end else begin
      Result := AByte;
   end;
end;

function ColorDarker(AColor: TColor; AFactor: extended): TColor;
var
   bRed, bGreen, bBlue, bType: byte;
begin
   bRed := (AColor and $FF);
   bGreen := (AColor shr 8) and $FF;
   bBlue := (AColor shr 16) and $FF;
   bType := (AColor shr 24) and $FF;
   if AFactor = 0 then begin
      Result := AColor;
      Exit;
   end;
   Result := (ReduceToByteMax(Round(bRed / AFactor))) or (ReduceToByteMax(Round(bGreen / AFactor)) shl 8) or (ReduceToByteMax(Round(bBlue / AFactor)) shl 16) or (bType shl 24);
end;

function ColorBrighter(AColor: TColor; AFactor: extended): TColor;
var
   bRed, bGreen, bBlue, bType: byte;
begin
   bRed := (AColor and $FF);
   bGreen := (AColor shr 8) and $FF;
   bBlue := (AColor shr 16) and $FF;
   bType := (AColor shr 24) and $FF;
   Result := (ReduceToByteMax(Round(bRed * AFactor))) or (ReduceToByteMax(Round(bGreen * AFactor)) shl 8) or (ReduceToByteMax(Round(bBlue * AFactor)) shl 16) or (bType shl 24);
end;

procedure GradHorizontal2(Canvas: TCanvas; Rect: TRect; AColor: TColor; AReverse: boolean);
var
   clFrom, clTo: TColor;
begin
   if AReverse then begin
      clTo := ColorBrighter(AColor, 1.5);
      clFrom := ColorDarker(AColor, 1.1);
   end else begin
      clFrom := ColorBrighter(AColor, 1.5);
      clTo := ColorDarker(AColor, 1.1);
   end;
   GradHorizontal(Canvas, Rect, clFrom, clTo);
end;

end.
