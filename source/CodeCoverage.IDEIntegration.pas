{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-20  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit CodeCoverage.IDEIntegration;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   SynEdit,
   SynGutter,
   MenuIntf,
   LazIDEIntf,
   SrcEditorIntf,
   DebuggerIntf,
   DbgIntfDebuggerBase,
   IDEMsgIntf,
   MacroIntf,
   IDEExternToolIntf,
   gdbmidebugger;

procedure Register;

implementation

uses
   Dialogs,
   CodeCoverage,
   CodeCoverage.Strings,
   CodeCoverage.SynGutter,
   CodeCoverage.FileReader;

type

   { TCodeCoverageIntegration }

   TCodeCoverageIntegration = class
   private
      class var FInstance: TCodeCoverageIntegration;
      class function Instance: TCodeCoverageIntegration;
   private
      FDebugger: TGDBMIDebugger;
      procedure DebuggerIdle(Sender: TObject);
      procedure DebuggerBreakPointHit({%H-}ADebugger: TDebuggerIntf; ABreakPoint: TBaseBreakPoint; var {%H-}ACanContinue: boolean);
      procedure DebuggerBeforeChangeState(ADebugger: TDebuggerIntf; AOldState: TDBGState);
      procedure DebuggerChangeState(ADebugger: TDebuggerIntf; OldState: TDBGState);
      procedure DebuggerCurrentLine(Sender: TObject; const ALocation: TDBGLocationRec);
      procedure DebuggerOutput(Sender: TObject; const AText: string);
      procedure DebuggerEvent(Sender: TObject; const ACategory: TDBGEventCategory; const AEventType: TDBGEventType; const AText: string);
      procedure DebuggerConsoleOutput(Sender: TObject; const AText: string);
      function DebuggerFeedback(Sender: TObject; const AText, AInfo: string; AType: TDBGFeedbackType; AButtons: TDBGFeedbackResults): TDBGFeedbackResult;
      procedure DebuggerException(Sender: TObject; const AExceptionType: TDBGExceptionType; const AExceptionClass: string; const AExceptionLocation: TDBGLocationRec;
         const AExceptionText: string; out AContinue: boolean);
   protected
      procedure DoCodeCoverageRun({%H-}Sender: TObject);
      procedure DoEditorCreated({%H-}Sender: TObject);
   protected
      procedure ShowGutterForAllWindows;
      procedure ShowGutterForWindow(AnEditor: TSourceEditorInterface);
   public
      class constructor Create;
      class destructor Destroy;
   public
      constructor Create;
      destructor Destroy; override;
      procedure CreateMainMenuSubMenu();
   end;

procedure Register;
begin
   TCodeCoverageIntegration.Instance.CreateMainMenuSubMenu();
end;

{ TCodeCoverageIntegration }

class function TCodeCoverageIntegration.Instance: TCodeCoverageIntegration;
begin
   if not Assigned(FInstance) then begin
      FInstance := TCodeCoverageIntegration.Create;
   end;
   Result := FInstance;
end;

procedure TCodeCoverageIntegration.DebuggerIdle(Sender: TObject);
begin

end;

procedure TCodeCoverageIntegration.DoCodeCoverageRun(Sender: TObject);
var
   s: string;
begin
   s := '$(TargetFile)';
   AddIDEMessage(mluHint, Format(rsCodeCoverageStatusMessageDebuggerName, [FDebugger.Caption]));
   if IDEMacros.SubstituteMacros(s) then begin
      FDebugger.FileName := s;
      AddIDEMessage(mluHint, '(temp) WarnOnTimeOut := true');
      TGDBMIDebuggerProperties(FDebugger.GetProperties).WarnOnTimeOut := True;
      AddIDEMessage(mluHint, '(temp) FDebugger.Init');
      try
         FDebugger.Init;
      except
         on E: Exception do begin
            AddIDEMessage(mluError, Format(rsCodeCoverageStatusMessageInitFailed, [E.Message]));
         end;
      end;
      AddIDEMessage(mluHint, '(temp) Run Next');
      FDebugger.Run;
   end;
end;

procedure TCodeCoverageIntegration.DoEditorCreated(Sender: TObject);
begin
   ShowGutterForAllWindows;
end;

procedure TCodeCoverageIntegration.DebuggerBreakPointHit(ADebugger: TDebuggerIntf; ABreakPoint: TBaseBreakPoint; var ACanContinue: boolean);
begin

end;

procedure TCodeCoverageIntegration.DebuggerBeforeChangeState(ADebugger: TDebuggerIntf; AOldState: TDBGState);
begin

end;

procedure TCodeCoverageIntegration.DebuggerChangeState(ADebugger: TDebuggerIntf; OldState: TDBGState);
begin

end;

procedure TCodeCoverageIntegration.DebuggerCurrentLine(Sender: TObject; const ALocation: TDBGLocationRec);
begin
   AddIDEMessage(mluImportant, ALocation.SrcFile + ' - ' + ALocation.FuncName);
end;

procedure TCodeCoverageIntegration.DebuggerOutput(Sender: TObject; const AText: string);
begin

end;

procedure TCodeCoverageIntegration.DebuggerEvent(Sender: TObject; const ACategory: TDBGEventCategory; const AEventType: TDBGEventType; const AText: string);
begin

end;

procedure TCodeCoverageIntegration.DebuggerConsoleOutput(Sender: TObject; const AText: string);
begin

end;

function TCodeCoverageIntegration.DebuggerFeedback(Sender: TObject; const AText, AInfo: string; AType: TDBGFeedbackType; AButtons: TDBGFeedbackResults): TDBGFeedbackResult;
begin

end;

procedure TCodeCoverageIntegration.DebuggerException(Sender: TObject; const AExceptionType: TDBGExceptionType; const AExceptionClass: string;
   const AExceptionLocation: TDBGLocationRec; const AExceptionText: string; out AContinue: boolean);
begin

end;

procedure TCodeCoverageIntegration.ShowGutterForAllWindows;
var
   i: integer;
begin
   for i := 0 to Pred(SourceEditorManagerIntf.SourceEditorCount) do begin
      ShowGutterForWindow(SourceEditorManagerIntf.SourceEditors[i]);
   end;
end;

procedure TCodeCoverageIntegration.ShowGutterForWindow(AnEditor: TSourceEditorInterface);
var
   syn: TSynEdit;
   bFound: boolean;
   i: integer;
begin
   try
      if AnEditor.EditorControl is TSynEdit then begin
         syn := TSynEdit(AnEditor.EditorControl);
         bFound := False;
         for i := 0 to Pred(syn.RightGutter.Parts.Count) do begin
            if syn.RightGutter.Parts[i] is TSynGutterCodeCoverage then begin
               bFound := True;
            end;
         end;
         if not bFound then begin
            with TSynGutterCodeCoverage.Create(syn.RightGutter.Parts) do begin
               CCReader := TCodeCoverageXMLReader.Create;
               CCReader.AssignLines(syn.Lines);
               CCReader.LoadFromFile(AnEditor.FileName + CodeCoverageFileExtension);
               Index := 0;
            end;
            with TSynGutterSeparator.Create(syn.RightGutter.Parts) do begin
               Index := 0;
            end;
         end;
      end;
   except
      on E: Exception do begin
      end;
   end;
end;

class constructor TCodeCoverageIntegration.Create;
begin
   FInstance := nil;
end;

class destructor TCodeCoverageIntegration.Destroy;
begin
   FInstance.Free;
end;

constructor TCodeCoverageIntegration.Create;
var s: string;
begin
   //s := 'C:\Development\lazarus-1.7\fpcbootstrap\gdb\$(TargetCPU)-$(TargetOS)\gdb.exe';
   //IDEMacros.SubstituteMacros(s);
   s := 'C:\Development\lazarus-1.7\fpcbootstrap\gdb\i386-win32\gdb.exe';
   FDebugger := TGDBMIDebugger.Create(s);
   FDebugger.OnBreakPointHit := @DebuggerBreakPointHit;
   FDebugger.OnBeforeState := @DebuggerBeforeChangeState;
   FDebugger.OnState := @DebuggerChangeState;
   FDebugger.OnCurrent := @DebuggerCurrentLine;
   FDebugger.OnDbgOutput := @DebuggerOutput;
   FDebugger.OnDbgEvent := @DebuggerEvent;
   FDebugger.OnException := @DebuggerException;
   FDebugger.OnConsoleOutput := @DebuggerConsoleOutput;
   FDebugger.OnFeedback := @DebuggerFeedback;
   FDebugger.OnIdle := @DebuggerIdle;
end;

destructor TCodeCoverageIntegration.Destroy;
begin
   FDebugger.Done;
   FDebugger.Free;
   inherited Destroy;
end;

procedure TCodeCoverageIntegration.CreateMainMenuSubMenu;
begin
   RegisterIDEMenuCommand(itmRunnning, 'TCodeCoverageRun', rsCodeCoverageMenuItemRun, @DoCodeCoverageRun);
   SourceEditorManagerIntf.RegisterChangeEvent(semEditorCreate, @DoEditorCreated);
   // TODO : find proper place to unregister, or do not unregister at all...
end;

end.
