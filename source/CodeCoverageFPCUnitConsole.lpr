{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-18  --  ---  Renamed from ram PepiMKCodeCoverageTestsFPConsole to PepiMKCodeCoverageTestsFPConsole
// 2017-06-18  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

program CodeCoverageFPCUnitConsole;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

uses
   Classes,
   consoletestrunner,
   CodeCoverage.FPCUnit.ConsoleTestRunner,
   CodeCoverage.TestCase,
   CodeCoverage.GDB.TestCase;

type

   { TMyTestRunner }

   TMyTestRunner = class(TCodeCoverageTestRunner)
   protected
      // override the protected methods of TTestRunner to customize its behavior
   end;

var
   Application: TMyTestRunner;

begin
   DefaultRunAllTests := true;
   Application := TMyTestRunner.Create(nil);
   Application.Initialize;
   Application.Run;
   Application.Free;
end.
