{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Wrapper around Lazarus GDBMI class customized for code coverage detection.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-22  pk  ---  Debugger action Run works now, thanks to customized monitors.
// 2017-06-21  pk  ---  First attempts at using TGDBMIDebugger
// *****************************************************************************
   )
}

unit CodeCoverage.Debugger;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   SysUtils,
   Classes,
   Forms,
   DebuggerIntf,
   DbgIntfDebuggerBase,
   FpDebugDebugger,
   GDBMIDebugger;

type
   TCodeCoverageDebuggerType = (
      ccdGDBMI,   // slow
      ccdFpDebug  // needs dwarf format, is in unstable/crashing state
      );

   TOnCodeCoverageLineDebugEvent = procedure(const AFilename: string; ALine: integer) of object;

   { TCodeCoverageCallStackList }

   TCodeCoverageCallStackList = class(TCallStackList)
   protected
      function NewEntryForThread(const AThreadId: integer): TCallStackBase; override;
   end;

   { TCodeCoverageCallStackMonitor }

   TCodeCoverageCallStackMonitor = class(TCallStackMonitor)
   protected
      function CreateCallStackList: TCallStackList; override;
   end;

   TCodeCoverageThreadsMonitor = class;

   { TCodeCoverageThreads }

   TCodeCoverageThreads = class(TThreads)
   private
      FMonitor: TCodeCoverageThreadsMonitor;
      FDataValidity: TDebuggerDataState;
   public
      constructor Create;
      function Count: integer; override;
      procedure Clear; override;
      procedure SetValidity(AValidity: TDebuggerDataState); override;
   end;

   { TCodeCoverageThreadsMonitor }

   TCodeCoverageThreadsMonitor = class(TThreadsMonitor)
   protected
      procedure DoStateEnterPause; override;
      function CreateThreads: TThreads; override;
      procedure RequestData;
   end;

   { TCodeCoverageDebugger }

   TCodeCoverageDebugger = class
   private
      FDebugger: TDebuggerIntf;
      FDebuggerExecutable: string;
      FDebuggerType: TCodeCoverageDebuggerType;
      FFullProtocol: TStringList;
      FOnLineDebug: TOnCodeCoverageLineDebugEvent;
      FProduceFullOutput: boolean;
      FThreadsMonitor: TCodeCoverageThreadsMonitor;
      FCallstackMonitor: TCodeCoverageCallStackMonitor;
      FLineProtocol: TStringList;
      FFilename: string;
      FFilenameLines: string;
      FLines: TFileStream;
      procedure DebuggerIdle(Sender: TObject);
      procedure DebuggerBreakPointHit({%H-}ADebugger: TDebuggerIntf; ABreakPoint: TBaseBreakPoint; var {%H-}ACanContinue: boolean);
      procedure DebuggerBeforeChangeState(ADebugger: TDebuggerIntf; AOldState: TDBGState);
      procedure DebuggerChangeState(ADebugger: TDebuggerIntf; OldState: TDBGState);
      procedure DebuggerCurrentLine(Sender: TObject; const ALocation: TDBGLocationRec);
      procedure DebuggerOutput(Sender: TObject; const AText: string);
      procedure DebuggerEvent(Sender: TObject; const ACategory: TDBGEventCategory; const AEventType: TDBGEventType; const AText: string);
      procedure DebuggerConsoleOutput(Sender: TObject; const AText: string);
      function DebuggerFeedback(Sender: TObject; const AText, AInfo: string; AType: TDBGFeedbackType; AButtons: TDBGFeedbackResults): TDBGFeedbackResult;
      procedure DebuggerException(Sender: TObject; const AExceptionType: TDBGExceptionType; const AExceptionClass: string; const AExceptionLocation: TDBGLocationRec;
         const AExceptionText: string; out AContinue: boolean);
      function CreateDebugger: TDebuggerIntf;
      procedure InitializeBreakpoints;
   public
      constructor Create;
      destructor Destroy; override;
      procedure Go(AFilename: string);
      property LineProtocol: TStringList read FLineProtocol;
      property FullProtocol: TStringList read FFullProtocol;
      property DebuggerExecutable: string read FDebuggerExecutable write FDebuggerExecutable;
      property OnLineDebug: TOnCodeCoverageLineDebugEvent read FOnLineDebug write FOnLineDebug;
      property ProduceFullOutput: boolean read FProduceFullOutput write FProduceFullOutput;
      property DebuggerType: TCodeCoverageDebuggerType read FDebuggerType;
   end;

implementation

uses
   TypInfo,
   CodeCoverage,
   CodeCoverage.Strings;

{ TCodeCoverageDebugger }

procedure TCodeCoverageDebugger.DebuggerIdle(Sender: TObject);
begin
   if FProduceFullOutput then begin
      FFullProtocol.Add('DebuggerIdle');
   end;
end;

procedure TCodeCoverageDebugger.DebuggerBreakPointHit(ADebugger: TDebuggerIntf; ABreakPoint: TBaseBreakPoint; var ACanContinue: boolean);
var
   sKind: string;
begin
   if FProduceFullOutput then begin
      WriteStr(sKind, ABreakPoint.Kind);
      FFullProtocol.Add('Breakpoint: ' + sKind);
   end;
   ACanContinue := True;
end;

procedure TCodeCoverageDebugger.DebuggerBeforeChangeState(ADebugger: TDebuggerIntf; AOldState: TDBGState);
var
   s: string;
begin
   if FProduceFullOutput then begin
      WriteStr(s, AOldState);
      FFullProtocol.Add('DebuggerBeforeChangeState: ' + s);
   end;
end;

procedure TCodeCoverageDebugger.DebuggerChangeState(ADebugger: TDebuggerIntf; OldState: TDBGState);
var
   s: string;
begin
   if FProduceFullOutput then begin
      WriteStr(s, OldState);
      FFullProtocol.Add('DebuggerChangeState: ' + s);
   end;
end;

procedure TCodeCoverageDebugger.DebuggerCurrentLine(Sender: TObject; const ALocation: TDBGLocationRec);
var
   csl: TCallStackList;
   csb: TCallStackBase;
   cse: TCallStackEntry;
   i, iEntry: integer;
   s: string;
begin
   if ALocation.SrcLine > -1 then begin
      s := Format('%s:%d', [ALocation.SrcFullName, ALocation.SrcLine]);
      if Assigned(FOnLineDebug) then begin
         FOnLineDebug(ALocation.SrcFullName, ALocation.SrcLine);
      end;
      FLineProtocol.Add(s);
      if FProduceFullOutput then begin
         FFullProtocol.Add(s);
      end;
      s := Format('%s=%d%s', [ALocation.SrcFullName, ALocation.SrcLine, LineEnding]);
      FLines.Write(s[1], Length(s));

   end;
   //csl := FDebugger.CallStack.CurrentCallStackList;
   //csb := csl.EntriesForThreads[FDebugger.Threads.CurrentThreads.CurrentThreadId];
   ////for i := 0 to Pred(csl.Count) do begin
   //   //FLineProtocol.Add(Format('Callstack %d', [i]));
   //   csb := csl.Entries[i];
   //   for iEntry := 0 to Pred(csb.Count) do begin
   //      cse := csb.Entries[iEntry];
   //      FLineProtocol.Add(Format('#%d: %s:%d - %s', [iEntry, cse.Source, cse.Line, cse.FunctionName]));
   //   end;
   //end;
end;

procedure TCodeCoverageDebugger.DebuggerOutput(Sender: TObject; const AText: string);
begin
   if FProduceFullOutput then begin
      FFullProtocol.Add('Output: ' + AText);
   end;
end;

procedure TCodeCoverageDebugger.DebuggerEvent(Sender: TObject; const ACategory: TDBGEventCategory; const AEventType: TDBGEventType; const AText: string);
begin
   if FProduceFullOutput then begin
      FFullProtocol.Add('Event: ' + AText);
   end;
end;

procedure TCodeCoverageDebugger.DebuggerConsoleOutput(Sender: TObject; const AText: string);
begin
   if FProduceFullOutput then begin
      FFullProtocol.Add('Console Output: ' + AText);
   end;
end;

function TCodeCoverageDebugger.DebuggerFeedback(Sender: TObject; const AText, AInfo: string; AType: TDBGFeedbackType; AButtons: TDBGFeedbackResults): TDBGFeedbackResult;
begin
   if FProduceFullOutput then begin
      FFullProtocol.Add('Feedback Text: ' + AText);
      FFullProtocol.Add('Feedback Info: ' + AInfo);
   end;
end;

procedure TCodeCoverageDebugger.DebuggerException(Sender: TObject; const AExceptionType: TDBGExceptionType; const AExceptionClass: string;
   const AExceptionLocation: TDBGLocationRec; const AExceptionText: string; out AContinue: boolean);
begin
   if FProduceFullOutput then begin
      FFullProtocol.Add('Exception: ' + AExceptionText);
   end;
   AContinue := True;
end;

function TCodeCoverageDebugger.CreateDebugger: TDebuggerIntf;
begin
   case FDebuggerType of
      ccdGDBMI:
      begin
         if Length(FDebuggerExecutable) = 0 then begin
            raise Exception.Create(rsCodeCoverageExceptionNoDebuggerExecutableSet);
         end;
         Result := TGDBMIDebugger.Create(FDebuggerExecutable);
      end;
      ccdFpDebug:
      begin
         Result := TFpDebugDebugger.Create('');
      end;
   end;
   Result.Threads.Monitor := FThreadsMonitor;
   Result.CallStack.Monitor := FCallstackMonitor;
   Result.OnBreakPointHit := @DebuggerBreakPointHit;
   Result.OnBeforeState := @DebuggerBeforeChangeState;
   Result.OnState := @DebuggerChangeState;
   Result.OnCurrent := @DebuggerCurrentLine;
   Result.OnDbgOutput := @DebuggerOutput;
   Result.OnDbgEvent := @DebuggerEvent;
   Result.OnException := @DebuggerException;
   Result.OnConsoleOutput := @DebuggerConsoleOutput;
   Result.OnFeedback := @DebuggerFeedback;
   Result.OnIdle := @DebuggerIdle;
end;

procedure TCodeCoverageDebugger.InitializeBreakpoints;
var
   dbp: TDBGBreakPoint;
begin
   dbp := FDebugger.BreakPoints.Add('c:\Development\Projects\CodingTools\laz-codecoverage-helper\source\CodeCoverage.GDB.TestCase.pas', 65);
   dbp.Enabled := True;
   dbp := FDebugger.BreakPoints.Add('c:\Development\Projects\CodingTools\laz-codecoverage-helper\source\CodeCoverage.GDB.TestCase.pas', 71);
   dbp.Enabled := True;
   dbp := FDebugger.BreakPoints.Add('c:\Development\Projects\CodingTools\laz-codecoverage-helper\source\CodeCoverage.GDB.TestCase.pas', 97);
   dbp.Enabled := True;
   dbp := FDebugger.BreakPoints.Add('c:\Development\Projects\CodingTools\laz-codecoverage-helper\source\CodeCoverage.GDB.TestCase.pas', 103);
   dbp.Enabled := True;
   dbp := FDebugger.BreakPoints.Add('c:\Development\Projects\CodingTools\laz-codecoverage-helper\source\HelloWorld.Greetings.TestCase.pas', 39);
   dbp.Enabled := True;
   dbp := FDebugger.BreakPoints.Add('TCODECOVERAGEGDBTESTCASE__TESTLISTALLUNITFILENAMES');
   dbp.Enabled := True;
end;

constructor TCodeCoverageDebugger.Create;
begin
   FLineProtocol := TStringList.Create;
   FFullProtocol := TStringList.Create;
   FThreadsMonitor := TCodeCoverageThreadsMonitor.Create;
   FCallstackMonitor := TCodeCoverageCallStackMonitor.Create;
   FDebugger := nil;
   ProduceFullOutput := False;
   FDebuggerType := ccdGDBMI;
end;

destructor TCodeCoverageDebugger.Destroy;
begin
   FLineProtocol.Free;
   FFullProtocol.Free;
   FDebugger.Free;
   FThreadsMonitor.Free;
   FCallstackMonitor.Free;
   inherited Destroy;
end;

procedure TCodeCoverageDebugger.Go(AFilename: string);
var
   i: integer;
begin
   FFilename := AFilename;
   FFilenameLines := AFilename + CodeCoverageExecutedLinesExtension;
   FLines.Free;
   FLines := TFileStream.Create(FFilenameLines, fmCreate or fmOpenWrite or fmShareDenyNone);
   try
   if FileExists(FFilenameLines) then begin
      DeleteFile(FFilenameLines);
   end;
   FDebugger.Free;
   FDebugger := CreateDebugger;
   FLineProtocol.Clear;
   FDebugger.Init;
   try
      i := 0;
      FDebugger.FileName := AFilename;
      FDebugger.WorkingDir := ExtractFilePath(AFilename);
      repeat
         Application.ProcessMessages; // ugly, remove after testing
         FDebugger.StepInto;
         if (i = 0) then begin
            InitializeBreakpoints;
         end;
         Inc(i);
      until (FDebugger.State in [dsStop]);
      FDebugger.Run;
   finally
      FDebugger.Done;
      //FLineProtocol.SaveToFile(AFilename + '.executedLines.lst');
   end;
   finally
      FreeAndNil(FLines);
   end;
end;

{ TCodeCoverageThreadsMonitor }

procedure TCodeCoverageThreadsMonitor.DoStateEnterPause;
begin
   inherited DoStateEnterPause;
   TCodeCoverageThreads(Threads).SetValidity(ddsUnknown);
end;

function TCodeCoverageThreadsMonitor.CreateThreads: TThreads;
begin
   Result := TCodeCoverageThreads.Create;
   TCodeCoverageThreads(Result).FMonitor := Self;
end;

procedure TCodeCoverageThreadsMonitor.RequestData;
begin
   if Supplier <> nil then
      Supplier.RequestMasterData;
end;

{ TCodeCoverageThreads }

constructor TCodeCoverageThreads.Create;
begin
   inherited Create;
   FDataValidity := ddsUnknown;
end;

function TCodeCoverageThreads.Count: integer;
begin
   if (FDataValidity = ddsUnknown) then begin
      FDataValidity := ddsRequested;
      FMonitor.RequestData;
   end;
   Result := inherited Count;
end;

procedure TCodeCoverageThreads.Clear;
begin
   FDataValidity := ddsUnknown;
   inherited Clear;
end;

procedure TCodeCoverageThreads.SetValidity(AValidity: TDebuggerDataState);
begin
   if FDataValidity = AValidity then begin
      exit;
   end;
   FDataValidity := AValidity;
   if FDataValidity = ddsUnknown then begin
      Clear;
   end;
end;

{ TCodeCoverageCallStackMonitor }

function TCodeCoverageCallStackMonitor.CreateCallStackList: TCallStackList;
begin
   Result := TCodeCoverageCallStackList.Create;
end;

{ TCodeCoverageCallStackList }

function TCodeCoverageCallStackList.NewEntryForThread(const AThreadId: integer): TCallStackBase;
begin
   Result := TCallStackBase.Create;
   Result.ThreadId := AThreadId;
   add(Result);
end;

end.
