{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-12  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit CodeCoverage.TestCase;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

{$IFNDEF FPCUnit}
{$IFNDEF FPTest}
{$DEFINE FPCUnit}
{$ENDIF FPTest}
{$ENDIF FPCUnit}

interface

uses
   Classes,
   SysUtils,
   {$IFDEF FPCUnit}
   fpcunit,
   testregistry,
   CodeCoverage.FPCUnit,
   {$ENDIF FPCUnit}
   {$IFDEF FPTest}
   TestFramework,
   CodeCoverage.FPTest,
   {$ENDIF FPTest}
   CodeCoverage;

type

   TCodeCoverageTestCase = class(TTestCase)
   published
      procedure FooBar();
   end;

implementation

(**
* @covers(RegisterCodeCoverageTestAssocation)
* @covers(TCodeCoverage.LoadUnitFilenames)
* @covers(TCodeCoverage.ParseSourceByFilename)
* @covers(TCodeCoverage.ParseTestByFilename)
* @covers(TCodeCoverage.ParseSources)
* @covers(TCodeCoverage.ParseTests)
* @covers(TCodeCoverage.FindCoveredThings)
* @covers(TCodeCoverage.UpdateClassesCoverage)
* @covers(TCodeCoverage.GetTestClassNames)
* @covers(TCodeCoverage.FindTestUnitFilenames)
* @covers(TCodeCoverage.FindSourceUnitFilenames)
* @covers(TCodeCoverage.DoCreateElementInSourceFile)
* @covers(TCodeCoverage.Create)
* @covers(TCodeCoverage.Destroy)
* @covers(TCodeCoverage.PreProcessCodeCoverage)
* @covers(TCodeCoverage.PostProcessCodeCoverage)
 *)
procedure TCodeCoverageTestCase.FooBar();
var
   c: TCodeCoverage;
begin
   {$IFDEF FPCUnit}
   c := TCodeCoverageFPCUnit.Create;
   {$ENDIF FPCUnit}
   {$IFDEF FPTest}
   c := TCodeCoverageFPTest.Create;
   {$ENDIF FPTest}
   try
      c.PreProcessCodeCoverage;
      c.PostProcessCodeCoverage;
      CheckTrue(c.TestCaseUnits.Count > 0, 'TCodeCoverage.TestCaseUnits.Count');
      CheckTrue(c.SourceUnits.Count > 0, 'TCodeCoverage.SourceUnits.Count');
   finally
      c.Free;
   end;
end;


initialization

   {$IFDEF FPCUnit}
   RegisterTest(TCodeCoverageTestCase);
   {$ENDIF FPCUnit}
   {$IFDEF FPTest}
   RegisterCodeCoverageTest(TCodeCoverageTestCase);
   {$ENDIF FPTest}

end.
