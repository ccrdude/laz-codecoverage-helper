{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Test case for simple Gnu Debugger file reader.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-16  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit CodeCoverage.GDB.TestCase;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

{$IFNDEF FPCUnit}
{$IFNDEF FPTest}
{$DEFINE FPCUnit}
{$ENDIF FPTest}
{$ENDIF FPCUnit}

interface

uses
   Classes,
   SysUtils,
   {$IFDEF FPCUnit}
   fpcunit,
   testregistry,
   {$ENDIF FPCUnit}
   {$IFDEF FPTest}
   TestFramework,
   CodeCoverage.FPTest,
   {$ENDIF FPTest}
   CodeCoverage.GDB;

type

   { TCodeCoverageGDBTestCase }

   TCodeCoverageGDBTestCase = class(TTestCase)
   published
      procedure TestListAllUnitFilenames;
      procedure TestGetUnitFullFilename;
   end;

implementation

{ TCodeCoverageGDBTestCase }

(**
* @covers(SectionNameToString)
* @covers(RVAToPointer)
* @covers(TGnuDebuggerFile.Create)
* @covers(TGnuDebuggerFile.Destroy)
* @covers(TGnuDebuggerFile.LoadFromFile)
* @covers(TGnuDebuggerFile.ProcessStabStr)
* @covers(TGnuDebuggerFile.ListUnitFilenames)
 *)
procedure TCodeCoverageGDBTestCase.TestListAllUnitFilenames;
var
   sFilename: string;
   dbg: TGnuDebuggerFile;
   sl: TStringList;
begin
   sFilename := ExtractFilePath(ParamStr(0)) + '..\testfiles\CodeCoverageFPCUnitConsole-windows.dbg';
   dbg := TGnuDebuggerFile.Create;
   try
      CheckTrue(FileExists(sFilename), Format('FileExists(%s)', [sFilename]));
      CheckTrue(dbg.LoadFromFile(sFilename), 'TGnuDebuggerFile.LoadFromFile');
      sl := TStringList.Create;
      try
         dbg.ListUnitFilenames(sl);
         CheckTrue(sl.Count > 0);
      finally
         sl.Free;
      end;
   finally
      dbg.Free;
   end;
end;

(**
* @covers(SectionNameToString)
* @covers(RVAToPointer)
* @covers(TGnuDebuggerFile.Create)
* @covers(TGnuDebuggerFile.Destroy)
* @covers(TGnuDebuggerFile.LoadFromFile)
* @covers(TGnuDebuggerFile.ProcessStabStr)
* @covers(TGnuDebuggerFile.GetUnitFullFilename)
 *)
procedure TCodeCoverageGDBTestCase.TestGetUnitFullFilename;
var
   sFilename, sUnitName: string;
   dbg: TGnuDebuggerFile;
   sl: TStringList;
begin
   sFilename := ExtractFilePath(ParamStr(0)) + '..\testfiles\CodeCoverageFPCUnitConsole-windows.dbg';
   dbg := TGnuDebuggerFile.Create;
   try
      CheckTrue(FileExists(sFilename), Format('FileExists(%s)', [sFilename]));
      CheckTrue(dbg.LoadFromFile(sFilename), 'TGnuDebuggerFile.LoadFromFile');
      dbg.StabStrings.SaveToFile(sFilename + '.stabstrings');
      sl := TStringList.Create;
      try
         sUnitName := 'CodeCoverage.GDB';
         CheckTrue(dbg.GetUnitFullFilename(sUnitName));
         CheckTrue(Pos(DirectorySeparator, sUnitName) > 0);
      finally
         sl.Free;
      end;
   finally
      dbg.Free;
   end;
end;

initialization

   {$IFDEF FPCUnit}
   RegisterTest(TCodeCoverageGDBTestCase);
   {$ENDIF FPCUnit}
   {$IFDEF FPTest}
   RegisterCodeCoverageTest(TCodeCoverageGDBTestCase);
   {$ENDIF FPTest}

end.
