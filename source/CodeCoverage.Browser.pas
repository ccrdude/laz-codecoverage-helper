unit CodeCoverage.Browser;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   FileUtil,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   ComCtrls;

type

   { TFormCodeCoverageBrowser }

   TFormCodeCoverageBrowser = class(TForm)
      pcFiles: TPageControl;
      procedure FormShow(Sender: TObject);
   private
      function AddTab(AFilename: string): TTabSheet;
   public
      procedure DisplayFolder(AFoldername: string);
   end;

var
   FormCodeCoverageBrowser: TFormCodeCoverageBrowser;

implementation

{$R *.lfm}

uses
   CodeCoverage,
   CodeCoverage.UnitFrame;

{ TFormCodeCoverageBrowser }

procedure TFormCodeCoverageBrowser.FormShow(Sender: TObject);
begin
   DisplayFolder(ParamStr(1));
end;

function TFormCodeCoverageBrowser.AddTab(AFilename: string): TTabSheet;
var
   f: TFrameCodeCoverage;
begin
   Result := TTabSheet.Create(Self);
   Result.Parent := pcFiles;
   Result.PageControl := pcFiles;
   Result.Caption := ExtractFileName(AFilename);
   f := TFrameCodeCoverage.Create(Self);
   f.Parent := Result;
   f.Align := alClient;
   f.ShowUnitCodeCoverage(AFilename);
   f.Name := 'FrameCodeCoverage' + IntToStr(Result.PageIndex);
end;

procedure TFormCodeCoverageBrowser.DisplayFolder(AFoldername: string);
var
   i: integer;
   sr: TSearchRec;
   sFilename: string;
begin
   i := FindFirst(IncludeTrailingPathDelimiter(AFoldername) + '*' + CodeCoverageFileExtension, faAnyFile, sr);
   if i = 0 then begin
      try
         while i = 0 do begin
            sFilename := Copy(sr.Name, 1, Length(sr.Name) - 4);
            AddTab(IncludeTrailingPathDelimiter(AFoldername) + sFilename);
            i := FindNext(sr);
         end;
      finally
         FindClose(sr);
      end;
   end;
end;

end.
