{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-23  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit HelloWorld.Greetings;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils;

function SayItDutch(): string;
function SayItGerman: string;
function SayItEnglish: string;
type

   { TSayItForeign }

   TSayItForeign = class
     public
       class function SayItPolish: string;
       function SayItRomanian: string;
   end;

implementation

function SayItDutch: string;
begin
   Result := 'Hallo Wereld';
end;

function SayItGerman: string;
begin
   Result := 'Hallo Welt';
end;

function SayItEnglish: string;
begin
   Result := 'Hello World';
end;

{ TSayItForeign }

class function TSayItForeign.SayItPolish: string;
begin
   Result := 'Witaj świecie';
end;

function TSayItForeign.SayItRomanian: string;
begin
   Result := 'Salut Lume';
end;

end.
