{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Code Coverage Browser Draft.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-21  --  ---  Renamed from ram CCBrowser to CCBrowser
// 2017-06-21  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

program CCBrowser;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
   cthreads, {$ENDIF} {$ENDIF}
   Interfaces, // this includes the LCL widgetset
   Forms,
   CodeCoverage.Browser,
   CodeCoverage,
   CodeCoverage.LineForm,
   CodeCoverage.Graphics,
   CodeCoverage.FileReader;

{$R *.res}

begin
   RequireDerivedFormResource := True;
   Application.Initialize;
   Application.CreateForm(TFormCodeCoverageBrowser, FormCodeCoverageBrowser);
   Application.Run;
end.
