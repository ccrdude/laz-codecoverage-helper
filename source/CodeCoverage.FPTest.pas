{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-18  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit CodeCoverage.FPTest;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   TestFramework,
   TestFrameworkIfaces,
   CodeCoverage,
   CodeCoverage.GDB;

type
   TTestCaseClass = class of TTestCase;

   { TCodeCoverageFPTest }

   TCodeCoverageFPTest = class(TCodeCoverage)
   private
      class var FTestClassList: TStringList;
   protected
      procedure GetTestClassNames(ANames: TStringList); override;
      {
        FindTestUnitFilenames overrides the original method by one that gathers
        information from the TestRegistry.

        @param(AGnuDebuggerFile A class holding information from the .gdb file.)
      }
      procedure FindTestUnitFilenames({%H-}AGnuDebuggerFile: TGnuDebuggerFile); override;
   public
      class constructor Create;
      class destructor Destroy;
   end;

procedure RegisterCodeCoverageTest(ATestClass: TTestCaseClass);

implementation

uses
   Windows,
   TypInfo;

procedure TCodeCoverageFPTest.GetTestClassNames(ANames: TStringList);
begin
   ANames.AddStrings(FTestClassList);
end;

procedure TCodeCoverageFPTest.FindTestUnitFilenames(AGnuDebuggerFile: TGnuDebuggerFile);
var
   it: IInterfaceList;
   i: integer;
   iu: IUnknown;
   sName: string;
   tp: TTestProject;
begin
   Exit;
   tp := TTestProject(TestProject(0));
   OutputDebugString(PChar('Project: ' + tp.ProjectName));
   OutputDebugString(PChar('Count: ' + tp.ToString));
end;

class constructor TCodeCoverageFPTest.Create;
begin
   FTestClassList := TStringList.Create;
end;

class destructor TCodeCoverageFPTest.Destroy;
begin
   FTestClassList.Free;
end;

procedure RegisterCodeCoverageTest(ATestClass: TTestCaseClass);
var i: ITestCase;
begin
   i := ATestClass.Suite;
   TestFramework.RegisterTest(i);
   OutputDebugString(PChar('RCCT: ' + (i as TTestCase).ClassName));
   TCodeCoverageFPTest.FTestClassList.Add(ATestClass.ClassName);
   CodeCoverage.RegisterCodeCoverageTestAssocation(ATestClass.ClassName, ATestClass.UnitName);
end;

end.
