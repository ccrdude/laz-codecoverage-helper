{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-18  --  ---  Renamed from ram CodeCoverageFPTestGUI to CodeCoverageFPTestGUI
// 2017-06-18  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

program CodeCoverageFPTestGUI;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
   cthreads, {$ENDIF} {$ENDIF}
   Classes,
   Forms,
   Interfaces,
   TestFramework,
   GuiTestRunner,
   CodeCoverage.TestCase,
   CodeCoverage.GDB.TestCase,
   CodeCoverage.FPTest;

begin
   Application.Title := 'CodeCoverageFPTestGUI';
   // Register all tests
   //TestFramework.RegisterTest(TCodeCoverageTestCase.Suite);
   //TestFramework.RegisterTest(TCodeCoverageGDBTestCase.Suite);
   with TCodeCoverageFPTest.Create do begin
      PreProcessCodeCoverage;
      Application.Initialize;
      RunRegisteredTests;
      PostProcessCodeCoverage;
      Free;
   end;
end.
