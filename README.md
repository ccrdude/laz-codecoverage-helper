# What is LazCodeCoverageHelper?

LazCodeCoverageHelper is a [Lazarus](http://www.lazarus-ide.org/) extension that improves [FPCUnit](http://wiki.lazarus.freepascal.org/fpcunit) and [FPTest](http://wiki.freepascal.org/FPTest) with a code coverage feature.

It tries to create output similar to the [PHPUnit Cove Coverage](https://phpunit.de/manual/current/en/code-coverage-analysis.html) feature.

# What is Code Coverage?

Quoting Wikipedia:
> In computer science, code coverage is a measure used to describe the degree to which the source code of a program is executed when a particular test suite runs. A program with high code coverage, measured as a percentage, has had more of its source code executed during testing which suggests it has a lower chance of containing undetected software bugs compared to a program with low code coverage. Many different metrics can be used to calculate code coverage; some of the most basic are the percentage of program subroutines and the percentage of program statements called during execution of the test suite.

# Current status

* Currently tested Windows-only.
* Method base code coverage works, check Installation and Usage sections.
* Line based code coverage works, but is way too slow, since it needs to "Step Into" all code lines.
* FreePascal gprof support (-pg) might help, but is broken on Windows due to Cygwin/MinGW issues.
* FpDebugDebugger is in alpha and crashes in current SVN version of Lazarus.
* For FPCUnit GuiTestRunner integration, the GuiTestRunner unit needs to be modified (see [Lazarus bug tracker issue #32052](https://bugs.freepascal.org/view.php?id=32052).
* For FPTest, there is no GuiTestRunner integration. 
* For FPTest, registration is not yet automatic. 
* Lazarus IDE integration works.

# Installation

1. Check out code from [the repository](https://gitlab.com/ccrdude/laz-codecoverage-helper).
2. Open `source/CodeCoveragePackage.lpk`, and click *Install*.
3. When ask, let the Lazarus IDE rebuild. 

# Usage

## General information

* For all test projects, open the *Project Options* (Shift+Ctrl+F11), go to *Compiler Options/Debugging*, and enable the options *Use external gdb debug symbols file (-Xg)*. Set *Type of debug info* to *Stabs (-gs)*. Without this, you need to manually specify test case units and tested units using the `SourceUnits` and `TestCaseUnits` properties.
* (WIP) If you want line based information, start the project using *Run Code Coverage Test* from the *Run* menu. This increases execution time a lot.

## Using Code Coverage with FPTest

As a user of the FPTest framework, you currently need to modify the main project file, and test case registration, either within the main project file, or each test case unit file, depending on where you call the existing `RegisterTest(...)`.

### Modify GUI test project file

1. Add unit `CodeCoverage.FPTest` to the uses clause.
2. Create an instance of `TCodeCoverageFPTest` and use `PreProcessCodeCoverage` and `PostProcessCodeCoverage`. Use the following example that extends the demo found in the FPTest source:

```
   with TCodeCoverageFPTest.Create do begin
      PreProcessCodeCoverage;
      Application.Initialize;
      RunRegisteredTests;
      PostProcessCodeCoverage;
      Free;
   end;
```


### Modify test case unit

These steps are required for each test case unit:
1. Add unit `CodeCoverage.FPTest` to the uses clause.
2. Replace `RegisterTest` with `RegisterCodeCoverageTest` in the `initialization` section, to register the test case for Code Coverage (this is intended to be done automatically similar to what already works in FPCUnit).
3. Add comments containing the `@covers()` keyword to each test method. Example:

```
(**
 * @covers(TSomething.Foo)
 *)
procedure TSomethingTestCase.TTestCase;
begin
   [...]
```

# Using Code Coverage with FPCUnit

You need to modify the test project, and each test case unit, to fully support the new code coverage feature. Then simply run the test and the visual form will be extended by code coverage tabs:

![Screenshot of FPCUnit GUI with Code Coverage tabs](doc/guitestrunner-codecoverage-1.png)

## Modify console test project file

If you have created the console runner using the Lazarus IDE, there are just two steps involved:
1. Add unit `CodeCoverage.FPCUnitConsoleTestRunner` to the uses clause.
2. Replace `TMyTestRunner = class(TTestRunner)` with `TMyTestRunner = class(TCodeCoverageTestRunner)`.

## Modify GUI test project file

There are a few steps involved in the test project file:
1. Add unit `CodeCoverage.FPCUnit.GuiTestRunner` to the uses clause.
2. Replace `TGUITestRunner` with `TCodeCoverageGUITestRunner` in the `Application.CreateForm` line.

```
program SomethingTests;

uses
   Interfaces, Forms, GuiTestRunner,
   CodeCoverage.FPCUnit.GuiTestRunner; // this was added

{$R *.res}

begin
   Application.Initialize;
   Application.CreateForm(TCodeCoverageGUITestRunner, TestRunner); // note the changed first parameter!
   Application.Run;
end.
```

## Modify test case unit

This step are required for each test case unit.
1. Added comments containing the `@covers()` keyword to each test method.

```
(**
 * @covers(TSomething.Foo)
 *)
procedure TSomethingTestCase.TTestCase;
begin
   [...]
```

# Viewing results

## Code Coverage Browser

This repository included a demo program called `CCBrowser` that will allow to browse folders specified as first runtime parameter, with a GUI similar to the FPCUnit one.

## Lazarus IDE

If you installed the Lazarus IDE package, the editor will have a new right gutter bar that shows coverage where applicable, and which can be clicked.
A popup window will show you more details about the coverage, for example which test methods did cover this line.

![Screenshot of editor gutter with popup](doc/guitestrunner-codecoverage-2.png)



